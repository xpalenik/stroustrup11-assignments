/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 */

#include "BarCode.h"
#include <string>

class CBarCodeEAN13: public CBarCode
{
    const std::string ParityEncodings[10] = {"111111", "110100", "110010", "110001", "101100", "100110", "100011", "101010", "101001", "100101"};

    const std::string BeginBar = "101";
    const std::string MiddleBar = "01010";
    const std::string& EndBar = BeginBar;

    const std::string LeftOddCoding[10] = {"0001101", "0011001", "0010011", "0111101", "0100011", "0110001", "0101111", "0111011", "0110111", "0001011"};
    const std::string LeftEvenCoding[10] = {"0100111", "0110011", "0011011", "0100001", "0011101", "0111001", "0000101", "0010001", "0001001", "0010111"};
    const std::string RightCoding[10] = {"1110010", "1100110", "1101100", "1000010", "1011100", "1001110", "1010000", "1000100", "1001000", "1110100"};

    int m_digits[12]; //pole obsahujúce dáta čiarového kódu (všetky čísla okrem posledného)
    int m_checksum; //kontrolná suma spočítaná z dátových číslic

    std::string m_left; //zakodovane cisla od 2 do 7 (using left encoding)
    std::string m_right; //zakodovane cisla od 8 do 12 (using right encoding)

    int computeCheckSum() const;
    void encode();
    void printBarCode(int& x, const int& offsetY, const int& scale, const int& height, const std::string& code) const;

public:

    CBarCodeEAN13(const int *eanCode);
    CBarCodeEAN13(const std::string& eanCode){
        parseInput(eanCode);
    }
    void zero();
    void print() const;
    bool parseInput(const std::string& data);
    bool isValid() const;
    void exportToSVG(std::size_t scale, std::size_t offsetX, std::size_t offsetY, bool svgHeader = false) const;
    int checkSum() const{
        return m_checksum;
    }
    const int* digits() const{
        return m_digits;
    }
};


