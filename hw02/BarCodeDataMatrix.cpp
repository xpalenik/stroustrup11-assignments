/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 */

#include "BarCodeDataMatrix.h"
#include <iostream>
#include <algorithm>
using namespace std;

CBarCodeDataMatrix::CBarCodeDataMatrix(const string &data, size_t dataMatrixSize):
                                       m_size(0),
                                       m_data(nullptr){
    if (dataMatrixSize<=0){
        m_data=nullptr;
        return;
    }

    m_size=dataMatrixSize;

    m_data=new bool*[dataMatrixSize];
    for(size_t i = 0; i < dataMatrixSize; ++i) {
        m_data[i] = new bool[dataMatrixSize];
    }

    zero();
    parseInput(data);
}


CBarCodeDataMatrix::~CBarCodeDataMatrix(){
    if (!isValid()){
        return;
    }

    for(size_t i = 0; i < m_size; ++i) {
        delete [] m_data[i];
    }

    delete [] m_data;
}


void CBarCodeDataMatrix::zero(){
    if (!isValid()){
        return;
    }

    for (size_t r=0;r<m_size;r++){
        for (size_t c=0;c<m_size;c++){
            m_data[r][c]=false;
        }
    }
}

bool CBarCodeDataMatrix::parseInput(const std::string& data){
    //can we write to m_data?
    if (!isValid()){
        return false;
    }

    zero();

    size_t row=0;
    size_t col=0;
    for (auto c: data){

        //check invalid characters
        if( c!='0' && c!='1' && !isspace(c) ){
            return false;
        }

        //skip white-spaces
        if (isspace(c)){
            continue;
        }

        m_data[row][col++]=(c=='1');

        if (col==m_size){
            if (row==m_size-1){
                return true;
            }

            row++;
            col=0;
        }
    }

    return false;
}

bool CBarCodeDataMatrix::isValid()const{
    return m_data!=nullptr;
}

void CBarCodeDataMatrix::print()const{
    if (!isValid()){
        return;
    }

    for (size_t row=0;row<m_size;row++){
        for (size_t col=0;col<m_size;col++){
            cout<<( (m_data[row][col]) ? "#" : " ");
        }

        //vystup NIE JE ukonceny znakom noveho riadku
        if (row!=m_size-1){
            cout<<endl;
        }

    }
}

void CBarCodeDataMatrix::exportToSVG(std::size_t scale, std::size_t offsetX, std::size_t offsetY, bool svgHeader)const{

    if (svgHeader){
        cout<<XMLVersion<<endl<<Doctype<<endl<<SVGHeader<<endl;
    }

    if (!isValid()){
        if (svgHeader){
            cout<<"</svg>";
        }

        return;
    }

    for (size_t row=0;row<m_size;row++){
        for (size_t col=0; col<m_size; col++){
                cout<<"\t<rect x=\""
                    <<offsetX+col*scale
                    <<"\" y=\""
                    <<offsetY+row*scale
                    <<"\" width=\""
                    <<scale
                    <<"\" height=\""
                    <<scale
                    <<"\" fill=\""
                    <<( (m_data[row][col]) ? "black" : "white" )
                    <<"\" />"
                    <<endl;
        }
    }

    if (svgHeader){
    cout<<"</svg>";
    }
}

CBarCodeDataMatrix::CBarCodeDataMatrix(const CBarCodeDataMatrix & other):
                                       m_size(other.m_size),
                                       m_data(new bool*[m_size]){
    for(size_t i = 0; i < m_size; ++i) {
        m_data[i] = new bool[m_size];
    }

    for (size_t row=0;row<m_size;row++){

        //copy(InputIterator first, InputIterator last, OutputIterator result);
        copy(other.m_data[row],  //bool* to first element on the given row
             other.m_data[row]+m_size, //bool* to the element after last element on the given row
             m_data[row]  //the destination, the pointer to the (first element of) given row on this->m_data
             );

    }
}

CBarCodeDataMatrix & CBarCodeDataMatrix::operator=(CBarCodeDataMatrix other){
    swap(m_size,other.m_size);
    swap(m_data,other.m_data);
    return *this;
}

