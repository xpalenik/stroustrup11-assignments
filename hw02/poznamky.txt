                                                                    !!!TODO!!!
                                                            nezabudni odstranit QT_DEBUG
                                                            valgrind!!!

CBarCodeEAN13 (linearny kod):

-> parseInput(): Treba robiť encode() ak sa načíta menej ako 12 číslic? (ja som implementoval ano) (cviciaci nevedel odpovedat)

"rozparsuje vstupný reťazec data podľa niekoľkých pravidiel a po načítaní spustí proces zakódovania, keďže sa obsah m_digits zmenil"
ale
"[bezprostredne?] Po načítaní prvých 12 číslic metóda skončí a vráti true. V opačnom prípade, pokiaľ reťazec neobsahoval potrebných 12 číslic a načítanie bolo predčasne ukončené, metóda vráti false."
ANO




CBarCodeDataMatrix (maticovy kod):

-> isValid(): Co ma robit isValid() v DataMatrix? Staci return m_data!=nullptr? Mam kontrolovat aj m_data[row]!=nullptr? S tym suvisi nasledovna otazka.
STACI m_data!=nullptr, lebo new by vyhodilo inak vynimku, pokial by nebol nothrow

-> exportToSvn(): Ma sa vypisovat kratky vstup? Alebo prazdny (nulovy) vstup? ASI ANO
"...pokiaľ reťazec neobsahoval potrebný počet hodnôt a načítanie bolo predčasne ukončené, metóda vráti false."
"Čiarový kód je exportovaný (vypísaný) iba v prípade, že je platný."
ANO

-> Je treba kopirovaci konstruktor a asignment operator? (rule of three komentar)
NIE


















Vyriesene:
Mozem presunut inicializaciu "const string CBarCodeEAN13::ParityEncodings[]" do hlavickoveho suboru? Ak nie, tak ako to mam spravit? ANO
Mozem presunut inicializaciu "const string CBarCode::XMLVersion" do "BarCode.h"? ANO
Ak BarCode.h includuje <string>, mozem v BarCode.cpp napisat len "using namespace std" bez "#include <string>? JE TO JEDNO, RADŠEJ NIE
Môžem použiť "using namespace CBarCodeEAN13"? NIE
Je rozdiel medzi isspace a iswspace? NIE JE PODSTATNY, isspace je OK
Môžu byť jednoriadkové funkcie definované v hlavičkovom súbore? ANO

Nepodstatne:
Môžem prepísať algoritmus parseInput cez stringstream (zjednodiší parsing whitespaceov)
