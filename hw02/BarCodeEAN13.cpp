/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 */

#include "BarCodeEAN13.h"

#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

//HOTOVO
CBarCodeEAN13::CBarCodeEAN13(const int *eanCode):m_checksum(0){

    //checks if eanCode is valid (not 0)
    if (eanCode==nullptr){
        zero();
        return;
    }

    //copy values from input
    for (int i=0;i<12;i++){
        m_digits[i]=eanCode[i];
    }

    //computes and saves the checksum
    m_checksum=computeCheckSum();

    //encodes the values from m_checksum to local variables
    encode();
}

//HOTOVO
void CBarCodeEAN13::zero(){
    //vynuluje dátové číslice
    for (int i=0;i<12;i++){
        m_digits[i]=0;
    }

    //vynuluje kontrolnú sumu
    m_checksum=0;

    //zakóduje aktuálnu hodnotu do čiarového kódu
    encode();
}

//HOTOVO
void CBarCodeEAN13::print() const{
    for (const auto digit:m_digits){
        cout<<digit;
    }

    cout<<m_checksum;
}

//HOTOVO
bool CBarCodeEAN13::parseInput(const std::string& data){

    zero();

    int loaded=0; //the number of loaded digits from data

    for (auto c: data){

        //check invalid characters
        if( !isdigit(c) && !isspace(c) ){
            break;
        }

        //skip white-spaces
        if (isspace(c)){
            continue;
        }

        //save the character (c-'0' converts digit[char] to digit[int])
        m_digits[loaded++]=c-'0';

        //end if enough digits found
        if (loaded==12){
            break;
        }

    }

    m_checksum=computeCheckSum(); //checksum will ALWAYS be computed
    encode(); //it IS ALWAYS necessary (see poznamky.txt)

    return loaded==12;
}

//HOTOVO
int CBarCodeEAN13::computeCheckSum() const{

    int sum=0;
    bool multiplyBy3=false;

    for (const auto i:m_digits){
        sum += multiplyBy3 ? 3*i : i;
        multiplyBy3 = !multiplyBy3;
    }

    return ( 10 - (sum%10) ) % 10;
}

//HOTOVO
bool CBarCodeEAN13::isValid() const{

    for (const auto digit:m_digits){
        if (digit<0 || digit>9){
            //cout<<"digit out of range\n";
            return false;
        }
    }

    if (m_checksum!=computeCheckSum()){
        //cout<<"invalid checksum\n";
        return false;
    }

    return true;
}

//HOTOVO
void CBarCodeEAN13::encode(){

    if (!isValid()){
        return;
    }

    m_left.clear();
    m_right.clear();

    //the correct left encoding type according to the first digit
    stringstream grid(ParityEncodings[m_digits[0]]);

    //print();
    //cout<<endl;
    //cout<<grid.str();
    //cout<<endl;

    char odd;
    for (int i=1;i<=6;i++){
        grid>>odd;
        //cout<<odd<<endl;

        if (odd=='1'){
            //cout<<LeftOddCoding[m_digits[i]]<<"("<<m_digits[i]<<")"<<endl;
            m_left+=LeftOddCoding[m_digits[i]];
        } else {
            //cout<<LeftEvenCoding[m_digits[i]]<<"("<<m_digits[i]<<")"<<endl;
            m_left+=LeftEvenCoding[m_digits[i]];
        }
    }

    //the right encoding part
    for (int i=7;i<=11;i++){
        m_right+=RightCoding[m_digits[i]];
    }

    m_right+=RightCoding[m_checksum];
}

//HOTOVO
void CBarCodeEAN13::exportToSVG(std::size_t scale, std::size_t offsetX, std::size_t offsetY, bool svgHeader) const{

    if (svgHeader){
        cout<<XMLVersion<<endl<<Doctype<<endl<<SVGHeader<<endl;
    }

    if (!isValid()){
        if (svgHeader){
            cout<<"</svg>";
        }

        return;
    }

    const int width=scale*95;
    const int heightLong=floor(0.65*width);
    const int heightShort=floor(0.9*heightLong);

    int x=offsetX;
    printBarCode(x, offsetY, scale, heightLong, BeginBar);
    printBarCode(x, offsetY, scale, heightShort, m_left);
    printBarCode(x, offsetY, scale, heightLong, MiddleBar);
    printBarCode(x, offsetY, scale, heightShort, m_right);
    printBarCode(x, offsetY, scale, heightLong, EndBar);

    if (svgHeader){
    cout<<"</svg>";
    }
}

//HOTOVO
void CBarCodeEAN13::printBarCode(int& x, const int& offsetY, const int& scale, const int& height, const string& code) const{
    for (const auto fill:code){
        cout<<"\t<rect x=\""
            <<x
            <<"\" y=\""
            <<offsetY
            <<"\" width=\""
            <<scale
            <<"\" height=\""
            <<height
            <<"\" fill=\""
            <<( (fill=='1') ? "black" : "white" )
            <<"\" />"
            <<endl;
        x+=scale;
    }
}
