/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 */
#include "BarCode.h"

class CBarCodeDataMatrix : public CBarCode{

    std::size_t m_size;
    bool** m_data;

public:
    CBarCodeDataMatrix(const std::string& data, std::size_t dataMatrixSize);
    CBarCodeDataMatrix(const CBarCodeDataMatrix & other);
    CBarCodeDataMatrix & operator=(CBarCodeDataMatrix other);
    virtual ~CBarCodeDataMatrix();

    void zero();
    void print()const;
    bool isValid()const;
    bool parseInput(const std::string& data);
    void exportToSVG(std::size_t scale, std::size_t offsetX, std::size_t offsetY, bool svgHeader = false)const;
    std::size_t size()const{
        return m_size;
    }
};
