/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "BarCode.h"
#include "BarCodeEAN13.h"
#include "BarCodeDataMatrix.h"
#include <string>
#include <cstring>
using namespace std;

/*
//Visual tests

TEST_CASE("head"){
    cout<<CBarCode::XMLVersion<<endl<<CBarCode::Doctype<<endl<<CBarCode::SVGHeader<<endl;
}

TEST_CASE("visual EAN13 - example1"){
    const string example="9788021048577";
    CBarCodeEAN13 e(example);
    e.exportToSVG(4,40,60,false);
}

TEST_CASE("visual EAN13 - example2"){
    const string example="330721166750";
    CBarCodeEAN13 e(example);
    e.exportToSVG(5,500,60,false);
}

TEST_CASE("visual BARCODE"){
    CBarCodeDataMatrix o(string("101011 100101 110100 101011 100010 111111"),6);
    o.exportToSVG(40,40,400,false);
}

TEST_CASE("end"){
    cout<<"</svg>";
}
*/

//Visual print
TEST_CASE("Visual print"){
    CBarCodeDataMatrix dm("101011 100101 110100 101011 100010 111111",6);
    //dm.print();

    CBarCodeEAN13 ean("330721166750");
    //ean.print();
}


//CBarCodeDataMatrix tests

TEST_CASE("Copy constructor and assignment operator in DataMatrix"){
    CBarCodeDataMatrix dm1(string("101011 100101 110100 101011 100010 111111"),6);
    CBarCodeDataMatrix dm2(dm1);
    CBarCodeDataMatrix dm3(string("101"),6);

    dm1.zero();

    //dm2.print();
    //cout<<endl<<endl;

    dm3=dm2;
    //dm3.print();
    //cout<<endl<<endl;

    dm3=dm3;
    //dm3.print();
    //cout<<endl<<endl;
}

TEST_CASE("void CBarCodeDataMatrix::zero()"){
    CBarCodeDataMatrix o(string("101011 100101 110100 101011 100010 111111"),6);

    SECTION("zero()"){
        o.zero();

        for (size_t row=0;row<o.m_size;row++){
            for (size_t col=0;col<o.m_size;col++){
                REQUIRE(o.m_data[row][col]==false);
            }
        }
    }
}

TEST_CASE("bool CBarCodeDataMatrix::isValid()const"){
    CBarCodeDataMatrix o(string(""),0);
    REQUIRE_FALSE(o.isValid());
}

TEST_CASE("bool CBarCodeDataMatrix::parseInput(const std::string& data)"){
    CBarCodeDataMatrix o(string("101011 100101 110100 101011 100010 111111"),6);

    SECTION("parseInput()"){
        REQUIRE_FALSE(o.parseInput(string("")));
        for (size_t row=0;row<o.m_size;row++){
            for (size_t col=0;col<o.m_size;col++){
                REQUIRE(o.m_data[row][col]==false);
            }
        }

        REQUIRE_FALSE(o.parseInput(string("1010")));
        for (size_t row=0;row<o.m_size;row++){
            for (size_t col=0;col<o.m_size;col++){
                if ( row==0 && (col==0 || col==2) ){
                    REQUIRE(o.m_data[row][col]==true);
                } else {
                    REQUIRE(o.m_data[row][col]==false);
                }
            }
        }

        REQUIRE_FALSE(o.parseInput(string("1x")));
        for (size_t row=0;row<o.m_size;row++){
            for (size_t col=0;col<o.m_size;col++){
                if (row==0 && col==0){
                    REQUIRE(o.m_data[row][col]==true);
                } else {
                    REQUIRE(o.m_data[row][col]==false);
                }
            }
        }

        REQUIRE_FALSE(o.parseInput(string("x")));
        for (size_t row=0;row<o.m_size;row++){
            for (size_t col=0;col<o.m_size;col++){
                REQUIRE(o.m_data[row][col]==false);
            }
        }
    }
}

TEST_CASE("std::size_t CBarCodeDataMatrix::size()const"){
    CBarCodeDataMatrix o(string("101011 100101 110100 101011 100010 111111"),6);

    REQUIRE(o.size()==6);
    REQUIRE(o.m_size==6);
}

/*
public:

    CBarCodeDataMatrix(const std::string& data, std::size_t dataMatrixSize);
    CBarCodeDataMatrix(const CBarCodeDataMatrix & other);
    CBarCodeDataMatrix & operator=(CBarCodeDataMatrix other);
    virtual ~CBarCodeDataMatrix();

    void zero();
    void print()const;
    bool isValid()const;
    bool parseInput(const std::string& data);
    void exportToSVG(std::size_t scale, std::size_t offsetX, std::size_t offsetY, bool svgHeader = false)const;
    std::size_t size()const{
        return m_size;
    }
*/









//CBarCodeEAN13 tests

TEST_CASE("CBarCodeEAN13::CBarCodeEAN13(const int *eanCode)"){
    int n[]={1,2,3};
    CBarCodeEAN13 c(n);
}

TEST_CASE("void CBarCodeEAN13::encode()"){
    const string example="330721166750";
    CBarCodeEAN13 e(example);

    SECTION("Valid example"){
        e.encode();
        REQUIRE(e.m_left=="011110100011010010001001101101100110011001");
        REQUIRE(e.m_right=="101000010100001000100100111011100101100110");
    }
}

TEST_CASE("bool CBarCodeEAN13::isValid() const"){
    CBarCodeEAN13 valid1("123456789012");

    SECTION("Valid"){
        REQUIRE(valid1.isValid());
        valid1.encode();
    }

    SECTION("Invalid"){
        int i1[12]={-2,5,-8,5,6,8,5,4,6,9,8,7};
        CBarCodeEAN13 io(i1);
        REQUIRE(!io.isValid());
    }
}

TEST_CASE("bool CBarCodeEAN13::parseInput(const std::string& data)"){
    const string c1="123456789012",
                 c2="1 2\n\t\t\n  3456789012",
                 c3="123456 789\t0123456",
                 s1="123d",
                 s2="1 2  3",
                 i1="abc",
                 i2="\n";

    CBarCodeEAN13 o("");

    SECTION("Correct input"){

        REQUIRE(o.parseInput(c1));
        REQUIRE(o.m_checksum==o.computeCheckSum());

        REQUIRE(o.parseInput(c2));
        REQUIRE(o.m_checksum==o.computeCheckSum());

        REQUIRE(o.parseInput(c3));
        REQUIRE(o.m_checksum==o.computeCheckSum());
    }

    SECTION("Short input"){

        REQUIRE(o.parseInput(s1)==false);
        REQUIRE(o.m_checksum==o.computeCheckSum());
        REQUIRE(o.m_left=="001001101111010100111000110101001110100111");
        REQUIRE(o.m_right=="111001011100101110010111001011100101110010");

        /*
        //code to print out the part encoded with left encoding

        for(auto d:o.m_digits){
            cout<<d;
        }

        cout<<o.m_checksum<<endl;

        int i=0;
        for (auto c:o.m_left){
            cout<<c;

            i=++i%7;
            if (i==0){
                cout<<" ";
            }
        }
        */

        REQUIRE(o.parseInput(s2)==false);
        REQUIRE(o.m_checksum==o.computeCheckSum());
        REQUIRE(o.m_left=="001001101111010100111000110101001110100111");
        REQUIRE(o.m_right=="111001011100101110010111001011100101110010");

    }

    SECTION("Invalid input"){
        REQUIRE(o.parseInput(i1)==false);
        REQUIRE(o.m_checksum==o.computeCheckSum());
        REQUIRE(o.m_left=="000110100011010001101000110100011010001101");
        REQUIRE(o.m_right=="111001011100101110010111001011100101110010");

        REQUIRE(o.parseInput(i2)==false);
        REQUIRE(o.m_checksum==o.computeCheckSum());
        REQUIRE(o.m_left=="000110100011010001101000110100011010001101");
        REQUIRE(o.m_right=="111001011100101110010111001011100101110010");
    }

}

TEST_CASE("int CBarCodeEAN13::computeCheckSum()"){
    const string example="330721166750";

    CBarCodeEAN13 o("");

    SECTION("Test from the example"){
        REQUIRE(o.parseInput(example));
        REQUIRE(o.computeCheckSum()==1);
    }
}

/*
    int computeCheckSum() const;
    void encode();
    void printBarCode(int& x, const int& offsetY, const int& scale, const int& height, const std::string& code) const;

public:

    CBarCodeEAN13(const int *eanCode);
    CBarCodeEAN13(const std::string& eanCode){
        parseInput(eanCode);
    }
    void zero();
    void print() const;
    bool parseInput(const std::string& data);
    bool isValid() const;
    void exportToSVG(std::size_t scale, std::size_t offsetX, std::size_t offsetY, bool svgHeader = false) const;
    int checkSum() const{
        return m_checksum;
    }
    const int* digits() const{
        return m_digits;
    }
*/
