#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "bigunsigned.h"

//because of stupid design, works only for CELL_TYPE = unsigned char
using CELL_TYPE = unsigned char;

size_t required_cells(size_t bytes, const size_t CELL_SIZE = sizeof(CELL_TYPE)){
    if (CELL_SIZE>=bytes){
        return 1;
    }

    if (bytes%CELL_SIZE){
        return bytes/CELL_SIZE+1;
    } else {
        return bytes/CELL_SIZE;
    }
}

TEST_CASE("1 byte min"){
    SECTION("BigUnsigned()"){
        BigUnsigned<CELL_TYPE> zero;
        REQUIRE(zero._integers.size()==1);
        REQUIRE(zero._integers.at(0)==0);
    }

    SECTION("BigUnsigned(int)"){
        BigUnsigned<CELL_TYPE> zero(0);
        REQUIRE(zero._integers.size()==1);
        REQUIRE(zero._integers.at(0)==0);
    }
}

TEST_CASE("1 byte max"){

    SECTION("BigUnsigned(int)"){
        BigUnsigned<CELL_TYPE> max(
                    std::numeric_limits<unsigned char>::max()
                    );
        REQUIRE(max._integers.size()==1);
        REQUIRE(max._integers.at(0)==std::numeric_limits<unsigned char>::max());
    }
}

TEST_CASE("2 bytes min"){
    SECTION("BigUnsigned(int)"){
        BigUnsigned<CELL_TYPE> max_plus_one(
                    1<<8 //std::numeric_limits<unsigned char>::max()+1
                    );

        REQUIRE(required_cells(2) == max_plus_one._integers.size());
        REQUIRE(max_plus_one._integers.at(0)==0);
        REQUIRE(max_plus_one._integers.at(1)==1);
    }
}

TEST_CASE("3 bytes min"){
    SECTION("BigUnsigned(int)"){
        BigUnsigned<CELL_TYPE> max_plus_one(
                    1<<16
                    );
        REQUIRE(required_cells(3) == max_plus_one._integers.size());
        REQUIRE(max_plus_one._integers.at(0)==0);
        REQUIRE(max_plus_one._integers.at(1)==0);
        REQUIRE(max_plus_one._integers.at(2)==1);
    }
}

TEST_CASE("4 bytes min"){
    SECTION("BigUnsigned(int)"){
        BigUnsigned<CELL_TYPE> max_plus_one(
                    1<<24
                    );
        REQUIRE(required_cells(4) == max_plus_one._integers.size());
        REQUIRE(max_plus_one._integers.at(0)==0);
        REQUIRE(max_plus_one._integers.at(1)==0);
        REQUIRE(max_plus_one._integers.at(2)==0);
        REQUIRE(max_plus_one._integers.at(3)==1);
    }
}

TEST_CASE("5 bytes min"){
    SECTION("BigUnsigned(unsigned long long)"){
        BigUnsigned<CELL_TYPE> max_plus_one(
                    static_cast<unsigned long long>(1)<<32
                    );
        REQUIRE(required_cells(5) == max_plus_one._integers.size());
        REQUIRE(max_plus_one._integers.at(0)==0);
        REQUIRE(max_plus_one._integers.at(1)==0);
        REQUIRE(max_plus_one._integers.at(2)==0);
        REQUIRE(max_plus_one._integers.at(3)==0);
        REQUIRE(max_plus_one._integers.at(4)==1);
    }
}
