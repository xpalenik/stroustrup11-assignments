#ifndef BIGUNSIGNED_H
#define BIGUNSIGNED_H
#include <vector>
#include <iostream>
using std::cerr;
using std::hex;
using std::uppercase;

//disables messages from cerr
#define cerr if (false) {} else std::cerr

template <typename I>
class BigUnsigned{
public: //REMOVE ME AFTER TESTING!!!
    const size_t CELL_SIZE=sizeof(I);
    const size_t BYTE_SIZE=8;
    std::vector<I> _integers;

    /**
     * Counts the minimal number of bytes required to store data type T.
     * (inspired by http://stackoverflow.com/a/2274499/1459339)
     * @param variable a variable of data type T
     * @return minimal number of bytes to store the variable
     */
    template <typename T>
    size_t bytes_required(T variable){
        size_t n = 0;
        while (variable != 0) {
            variable >>= BYTE_SIZE;
            n ++;
        }
        return n;
    }

public:
    BigUnsigned(){
        _integers.push_back(0);
    }

    BigUnsigned(int value)
        :BigUnsigned(static_cast<unsigned long long>(value)){
    }

    BigUnsigned(unsigned long long value){
        cerr<<"inserting '"<<value<<"'...\n";
        cerr<<"cell size is: '"<<CELL_SIZE<<"'\n";
        cerr<<"bytes_required is: '"<<bytes_required(value)<<"'\n";
        cerr<<"sizeof(value) is: '"<<sizeof(value)<<"'\n";

        //insert all cells except last
        while (CELL_SIZE < bytes_required(value)){
            cerr<<"inserting into cell\n";
            _integers.push_back( value & (0xff * CELL_SIZE) );
            value >>= BYTE_SIZE * CELL_SIZE;
        }
        //insert last cell
        cerr<<"inserting last cell...\n";
        _integers.push_back( value  );

        cerr<<"value.\n";
    }

    friend std::ostream& operator<<(std::ostream& out, const BigUnsigned& bu){
        for (const I integer : bu._integers){
            //unary operator '+' promotes [char] to printable integer type
            //see https://isocpp.org/wiki/faq/input-output#print-char-or-ptr-as-number
            out<<"'"<<hex<<uppercase<<+integer<<"'"<<std::endl;
        }
        return out;
    }
};

#endif // BIGUNSIGNED_H
