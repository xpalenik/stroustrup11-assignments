TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -pedantic -Wall -Wextra -ggdb
SOURCES += \
    bigunsigned.cpp \
    tests.cpp

HEADERS += \
    bigunsigned.h
