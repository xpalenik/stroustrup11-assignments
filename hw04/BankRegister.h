#ifndef BANK_REGISTER_H
#define BANK_REGISTER_H

#include <map> //added
#include "Bank.h" //added

class Bank;

typedef unsigned int uint;

class BankRegister final
{
    std::map<uint,Bank*> m_banks; //added
    uint m_next_bankNumber=1; //added
public:
    BankRegister() = default;

    //Rule of three:
    ~BankRegister() = default;
    BankRegister (const BankRegister&) = delete;
    BankRegister& operator=(BankRegister) = delete;

    uint registerBank(Bank* bank);
    void unregisterBank(uint bankNumber);
    
    Bank* bankByNumber(uint bankNumber) const;
};

#endif
 
