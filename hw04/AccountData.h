/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#ifndef ACCOUNTDATA_H
#define ACCOUNTDATA_H

typedef unsigned int uint;

class AccountData
{
    uint m_accountNumber; //added
  //uint m_ownerNumber;   //WHY should I need it (according to commented code here)?

public:

    // Mozny konstruktor a interface:
    //AccountData(uint number, uint ownerNumber);

    //I used this constructor instead:
    AccountData(uint accountNumber)
        :m_accountNumber(accountNumber){
    }

    uint number() const{        //uncommented
        return m_accountNumber; //added
    }

    /*
    uint ownerNumber() const{ //copied from original assignment
        return m_ownerNumber; //added
    }
    */
};

#endif
