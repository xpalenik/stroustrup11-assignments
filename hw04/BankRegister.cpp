/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "BankRegister.h"
#include "BankExceptions.h" //added
#include <iostream> //added
using std::cerr;

uint BankRegister::registerBank(Bank* bank){
    if (! m_banks.emplace(m_next_bankNumber,
                          bank)
                 .second){
        cerr<<"Error saving Bank* in BankRegister.\n";
        cerr<<"A bank with the given number probably exists.\n";
    }

    return m_next_bankNumber++;
}

void BankRegister::unregisterBank(uint bankNumber){

    auto result = m_banks.find(bankNumber);

    if (result != m_banks.end()){
        //delete result->second; //CONSULTATION
        m_banks.erase(result);
    }

    throw NonexistentBank();
}

Bank* BankRegister::bankByNumber(uint bankNumber) const{

    auto result = m_banks.find(bankNumber);

    if ( result != m_banks.end() ){
        return result->second;
    }

    throw NonexistentBank();
}
