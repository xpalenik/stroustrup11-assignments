#ifndef ACCOUNTID_H
#define ACCOUNTID_H

typedef unsigned int uint;

class AccountID
{
    uint m_accountNumber;
    uint m_bankNumber;
public:
    AccountID(uint accountNumber, uint bankNumber):
        m_accountNumber(accountNumber),
        m_bankNumber(bankNumber){
    }

    uint accountNumber() const{
        return m_accountNumber;
    }
    uint bankNumber() const{
        return m_bankNumber;
    }
};

#endif
