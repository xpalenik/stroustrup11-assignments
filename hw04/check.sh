#!/bin/bash

echo -e "Adding gcc-4.8.2 module for AISA."
module add gcc-4.8.2 2>/dev/null

echo -e "svn update"
svn update
echo -e "removing showcase_main.cpp"
rm showcase_main.cpp 2>/dev/null

examples="tests" #the name of directory with commands
difference="difference" #the name of temporary file from diff

if [ ! -d ${examples} ]; then
	echo -e "Directory with examples not found."
	echo -e "Please create a directory with examples called ${examples}"
	exit
fi

if [ ! "$(ls -A ${examples})" ]; then
	echo -e "Directory with examples is empty."
	exit
fi

if [ $1 ]; then	        
	echo -e "Removing ./a, *.gch out out_correct"
	rm a.out *.gch out out_correct 2>/dev/null
	echo -e "Compiling"
	g++ -std=c++11 -pedantic -Wall -Wextra -Werror -O0 -g *.cpp *.h
fi

echo -e "Starting a check of example commands."

passed=true

for file in ${examples}/*
do
	if [ -d ${file} ]; then
	  continue;
	fi

	echo -n "Checking ${file}..."
	if [[ ${file} =~ skip$ ]]; then
	    if [ ! $2 ]; then 
	        echo -e "\033[0;33mskipped\033[0m"
	        continue;
            fi
        fi	

	cat ${file} | ./hw04  > out_correct
	cat ${file} | ./a.out > out
	diff out out_correct > ${difference}
	if [ -s ${difference} ]
	then
     	  passed=false
	  echo -e "\033[0;31mfailed\033[0m"
          echo -e "the difference is:"
	  cat difference
	else
	  echo -e "\033[0;32mok\033[0m"
	fi
done

echo -e "Removing the temporary files."
rm out out_correct

if $passed; then
  echo -e "\033[0;32mAll tests passed.\033[0m"
else
  echo -e "\033[0;31mSome test failed!\033[0m"
fi
