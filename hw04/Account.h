#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "AccountInterface.h"
#include "AccountData.h" //added
#include "Bank.h" //added

typedef unsigned int uint;

class BasicAccount : public AccountInterface
{
    uint m_balance=0;
    AccountData m_accountData; //added
    Bank* m_bank; //added
public:
    //This is IMHO a design flaw!
    // Mozny konstruktor:
    //BasicAccount(uint number, Bank* bank, Database* db);
    
    //I used this constructor instead:
    BasicAccount(uint accountNumber, Bank* bank)
        :m_accountData(accountNumber),
         m_bank(bank){
    }

    //Rule of three:
    ~BasicAccount() = default;
    BasicAccount (const BasicAccount&) = delete;
    BasicAccount& operator=(BasicAccount) = delete;

    void deposit(uint amount)override;
    void withdraw(uint amount)override;
    void transfer(uint amount, AccountID target)override;

    uint balance() const override{
        return m_balance;
    }

    uint number() const override{
        return m_accountData.number();
    }
};

#endif
