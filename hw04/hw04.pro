TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -Wall -Wextra -pedantic

SOURCES += main.cpp \
    Account.cpp \
    Bank.cpp \
    BankRegister.cpp \
    Customer.cpp \
    Database.cpp \
    Interactive.cpp

HEADERS += \
    Account.h \
    AccountData.h \
    AccountID.h \
    AccountInterface.h \
    Bank.h \
    BankExceptions.h \
    BankRegister.h \
    Customer.h \
    CustomerData.h \
    CustomerInterface.h \
    Database.h \
    Interactive.h

DISTFILES += \
    consultations.txt \
    worklog.txt \
    tests/bank_customer_account \
    tests/create \
    tests/hw04_chyba.skip \
    tests/transfer \
    tests/bank3customers6accounts6 \
    tests/exhaustive_including_invalid

