/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "BankRegister.h"
#include "Interactive.h"
#include "BankExceptions.h"
#include <iostream>
using namespace std;

int main(){
    BankRegister bankRegister;
    Interactive shell(&bankRegister);

    string command;
    cin>>command;

    while ((command != "quit") && (cin.good())){
        string arguments;
        getline(cin,arguments);

        try {
            shell.execute(command,arguments);
        } catch (const BankException& ex){
            cout << "Exception: " << ex.what() << endl;
        }

        cin>>command;
    }

    return 0;
}
