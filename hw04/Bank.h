#ifndef BANK_H
#define BANK_H

#include <string>

#include "Database.h"
#include "AccountID.h"

typedef unsigned int uint;

class BankRegister;
class CustomerInterface;
class AccountInterface;

class Bank
{
    std::string m_name; //added
    uint m_number; //added
    Database m_db; //added
    std::map<uint, CustomerInterface*> m_customers; //added
    std::map<uint, AccountInterface*> m_accounts; //added
    BankRegister* m_bankRegister; //added

public:
    Bank(std::string const& name, BankRegister* centralRegister); //done

    //Rule of three:
    virtual ~Bank(); //added, done
    Bank (const Bank&) = delete; //added, done
    Bank& operator=(Bank) = delete; //added, done

    AccountInterface* createAccount(uint ownerNumber); //added (allowed in assignment), done
    CustomerInterface* createCustomer(std::string const& name); //done

    CustomerInterface* customerByNumber(uint customerNumber); //done
    AccountInterface* accountByNumber(uint accountNumber); //done

    std::map<uint, CustomerInterface*> const& customers(){ //done
        return m_customers;
    }

    std::map<uint, AccountInterface*> const& accounts(){ //done
        return m_accounts;
    }

    void transfer(uint amount, uint source, AccountID target); //done
    
    std::string const& name() const{ //done
        return m_name;
    }

    uint number() const{ //done
        return m_number;
    }
};

#endif 
