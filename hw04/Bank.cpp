/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "Bank.h"
#include "BankRegister.h" //added
#include <string> //added
#include "BankExceptions.h" //added
#include "AccountInterface.h" //added (declares AccountInterface* in destructor)
#include "CustomerInterface.h" //added (declares CustomerInterface* in destructor)
#include "Customer.h" //added (declares BasicCustomer in createCustomer())
#include "Account.h" //added (declares BasicAccount in createAccount())
#include <iostream> //added
using std::cerr; //added

Bank::Bank(std::string const& name, BankRegister* centralRegister)
    :m_name(name),
     m_bankRegister(centralRegister){

    if (centralRegister==nullptr){
        throw BankException("BankRegister* is nullptr");
    }

    m_number = centralRegister->registerBank(this);
}

Bank::~Bank(){
    for (auto account:m_accounts){
        delete account.second;
    }

    for (auto customer:m_customers){
        delete customer.second;
    }
}


CustomerInterface* Bank::createCustomer(std::string const& name){
    CustomerData customerData = m_db.createCustomer(name);
    m_db.save(customerData);

    BasicCustomer* customer = new BasicCustomer(customerData,this);

    if (! m_customers.emplace(customerData.number(),
                              customer)
                     .second){
        cerr<<"Error saving new customer in Bank.\n";
        cerr<<"A customer with the given number probably exists.\n";
    }

    return customer;
}

AccountInterface* Bank::createAccount(uint ownerNumber){

    //throws NonexistentCustomer
    if (m_customers.find(ownerNumber) == m_customers.end()){
        throw NonexistentCustomer();
    }

    AccountData accountData = m_db.createAccount(ownerNumber);
    m_db.save(accountData);

    BasicAccount* account = new BasicAccount(accountData.number(), this); //parameters???

    if (! m_accounts.emplace(accountData.number(),
                             account)
                     .second){
        cerr<<"Error saving new account in Bank.\n";
        cerr<<"An account with the given number probably exists.\n";
    }

    return account;
}

CustomerInterface* Bank::customerByNumber(uint customerNumber){

    //throws NonexistentCustomer
    auto result = m_customers.find(customerNumber);

    if (result != m_customers.end()){
        return result->second;
    }

    throw NonexistentCustomer();
}

AccountInterface* Bank::accountByNumber(uint accountNumber){

    //throws NonexistentAccount
    auto result = m_accounts.find(accountNumber);

    if (result != m_accounts.end()){
        return result->second;
    }

    throw NonexistentAccount();
}

void Bank::transfer(uint amount, uint source, AccountID target){

    //throws NonexistentAccount
    AccountInterface* sourceAccount =
            accountByNumber(source);

    if ( m_number == target.bankNumber() ){ //both accounts in this bank
        if ( source != target.accountNumber() ){ //to another account

            //throws NonexistentAccount
            AccountInterface* targetAccount =
                    accountByNumber(target.accountNumber());

            //throws InsufficientFunds
            sourceAccount->withdraw(amount);

            targetAccount->deposit(amount);
        } else {
            if (sourceAccount->balance()<amount){
                throw InsufficientFunds();
            }
        }

        return;
    }

    //throws NonexistentBank
    Bank* targetBank = m_bankRegister->bankByNumber(
                            target.bankNumber()
                       );

    //throws NonexistentAccount
    AccountInterface* targetAccount =
            targetBank->
            accountByNumber(target.accountNumber());

    //throws InsufficientFunds
    sourceAccount->withdraw(amount);

    targetAccount->deposit(amount);
}
