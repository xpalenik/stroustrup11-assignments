Otázky na konzultáciu s prednášajúcim (Nikola Beneš):

1. Vo funkcii Bank::createAccount() nie je mozne oznamit BasicCustomer, ze si ma pridat AccountInterface* do svojho vektora. Nie je to mozne nijak vykonat, lebo BasicCustomer na to nema verejnu metodu. Predpokladam teda, ze ucet sa vzdy bude vytvarat volanim BasicCustomer::createAccount a nie Bank::createAccount!!! Môžem to predpokladať, alebo sa to dá riešiť inak?

2. Potrebujem CustomerData::m_accountNumbers a CustomerData::accounts()?
čísla účtov viem predsa získať (vždy zotriedené) z BasicCustomer::m_accounts

3. Rozmysli si Rule of three pre všetky triedy (kde zakázať kopírovanie?).
poradca (nebol si istý, nevysvetlil prečo): BasicCustomer, BasicAccount, Bank

4. Vo funkcií "AccountData Database::createAccount(uint ownerNumber)" nevyužívam parameter ownerNumber a generuje mi warning unused... (fix cez ownerNumber++).
predpokladám, že ho mám predať AccountData
načo potrebujem uint AccountData::ownerNumber a AccountData::ownerNumber()?

5. transfer 99999 1/1 666/2 -> ktoru vynimku vypisat ako prvu? podla wiki je to jedno.

7. treba riešiť vkladanie záporného čísla?
poradca: pozrieť referenčnú implementáciu
ja: pozrel som, porozmýšľal a asi netreba
---------------------------------------------------------

Vyriešené:
1. Čo má vracať uint registerBank(Bank* bank)? Číslo práve registrovávanej banky?
-> ja: ANO

2. môže registerBank hodiť výnimku? Čo ak jej príde nullptr?
-> Bc. Michal Mikloš (fórum): netreba ošetrovať, ale je to dobrý programátorský štýl
...a preto som to ošetril

3. ako vraziť dynamické štruktúry do mapy (RAII)?
move semantics?
smart pointers?
 unique?shared?weak?
ukazatele
hodnotou
-> ja: zo zadania je zrejmé, že to musia byť ukazatele

4. parsing prikazov pomocou funktorov?
-> ja: good idea bro! done.

7. Konštruktor BasicCustomer by mal mat const parametre.
-> ja: nie, lebo const Bank* by nemohlo volať nekonštantné verejné metódy banky (createAccount()).
- Bank* aj tak nemôže meniť privátne atribúty banky

11. Môže mať BasicCustomer účty vo viacerých bankách?
-> ja: nie, ale v iných bankách môže byť zákazník s rovnakým číslom

12. Môže mať Bank viac databáz?
NIE
"Třída by měla obsahovat objekt třídy Database jakožto členskou proměnnou."
teda jeden objekt

10. nemali by byt vsetky countery (napr. BankRegister) static?
a)BankRegister.m_registered
nemusí (podľa mňa aj podľa študijného poradcu Mareka Osvalda)
pretože ak zanikne BankRegister tak zaniknú aj všetky banky a môže sa začať číslovať odznova
--> po dôkladnom prehliadnutí som nenašiel iné countery
(ešte však budem nejaké implementovať napr. v Database)
Database counter NESMIE byť statický!
Inak by sa v rôznych databázach rôznych bánk inkrementoval globálny counter spoločný pre každú banku.

17. Ošetrovať pretečenie counterov? (napr. volanie z BasicCustomer)
-> Bc. Michal Mikloš (fórum): netreba ošetrovať
...tak neošetrujem

18. Ošetrovať vyhodenie výnimky z new? (napr. volanie z BasicCustomer)
-> Bc. Michal Mikloš (fórum): netreba ošetrovať
...tak neošetrujem

19. Aké dátové štruktúry sú vhodné pre klienta a účet?
-> poradca: map
ja: zo zadania vyplýva, ze map
poradca: Možné a implicitne zoradené (červenočiernym stromom) sú set a map.
náhodný okoloidúci: Mapa je vhodnejšia!!!
poradca: prvý parameter je číslo uctu
poradca: druhý parameter je pointer
poradca: Pri vkladaní do mapy kopíruješ hodnotou -> utrpí performance. Ako riešiť?
poradca: Move sémantika -> zložité
poradca: Smart pointre -> ok, ale použi unique a nie share pointer
poradca: Kopírovať hodnotou -> najjednoduchšie, postačujúce a bezproblémové riešenie

20. Na čo si dať pozor?
poradca: vkladanie "aaa" tam kde sa vyžaduje int!!! -> netestuje sa
poradca: meno banky môže obsahovať viac slov -> použi getline()
poradca: pretečenie int -> nerieš (bude sa to riešiť až v hw06)

21. "vypsání čísel účtů ve vzestupném pořadí" -> comparator, funkčný objekt, sort, implicitne zoradená dátová štruktúra?
ja: map

22. "Vzpomeňte si na Rule of Three a zakažte kopírování a přiřazování u tříd kde tyto funkce nedávají smysl" - kde by to nemuselo dávať zmysel a prečo?
poradca (nebol si istý, nevysvetlil prečo): BasicCustomer, BasicAccount, Bank

23. kde je zoznam implicitne zoradených dátových štruktúr v C++11?
poradca: Nevie kde je explicitne, on pozná len set a map.

24. string compare vs strcmp
poradca: operator< a pod sa sprava rovnako ako ostatne -> použit!

---------------------------------------------------------
Zaujímavosti:

"brát konstantní referenci na int, char a další vestavěné datové typy může naopak výkonnostně uškodit"

Je dobrý programátorský štýl v .cpp znovu includovať súbory includované vo vloženom .h?
#include "InterfaceFoo.h" //includes string
#include <string> //should I do this?
using std::cout; //or this?
using namespace std; //or this?

Ktoré je lepší programátorský štýl?
#include <map> //added
#include "Bank.h" //added
alebo
#include "Bank.h" //added
#include <map> //added

Je ten class diagram dobre navrhnutý?

Čo ak vrátime const& a objekt zanikne?

Kto nastaví môj Object* na nullptr ak objekt zanikne? riesene v dokumentacii

Ako vypísať what() pri catch (...)?

Je dobrý programátorský štýl predanie výnimky z catch nejakej funkcií na spracovanie?

Post I wrote regarding BasicCustomer::accounts(), but was not sent:
"Vďaka, vyriešené. Chcem sa ešte spýtať na BasicCustomer::accounts(). Vzorové zdrojáky nás (v komentári) navádzajú k vector<uint> CustomerData::m_accountNumbers. Predpokladal som teda, že BasicCustomer::accounts() vráti aktuálne spočítanú množinu účtov (z Bank pomocou m_accountNumbers) hodnotou. Návratový typ BasicCustomer::accounts() je však predávaný referencou a preto nemôžem vrátiť spočítaný vektor (lokálnu premennú) hodnotou. Mohol by som ho dynamicky alokovať, ale tomu sa chcem vyhnúť. Jediné riešenie teda zrejme je vytvoriť vector<AccountInterface*> BasicCustomer::m_accounts a pri každom volaní BasicCustomer::accounts() ho vyprázdniť (clear), vygenerovať a vrátiť."
