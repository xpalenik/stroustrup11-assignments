/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "Customer.h"
#include <utility> //added
using std::pair; //added
using std::vector; //added

//---class BasicCustomer-->

BasicCustomer::BasicCustomer(const CustomerData& customerData, Bank *bank):
    m_customerData(customerData),
    m_bank(bank){
}

AccountInterface* BasicCustomer::createAccount(){
    AccountInterface* account = m_bank->createAccount(m_customerData.number());
    m_accounts.push_back(account);
    return account;
}

vector<AccountInterface*> const& BasicCustomer::accounts(){
    return m_accounts;
}

std::string const& BasicCustomer::name() const{
    return m_customerData.name();
}

uint BasicCustomer::number() const{
    return m_customerData.number();
}
