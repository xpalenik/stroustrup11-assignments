#ifndef CUSTOMERDATA_H
#define CUSTOMERDATA_H

#include <string> //added
#include <vector> //added

typedef unsigned int uint;

class CustomerData
{
    friend class BasicCustomer;

    uint m_number; //added
    std::string m_name; //added

public:

    //Mozny konstruktor a interface:
    CustomerData(uint number, std::string const& name): //uncommented
        m_number(number), m_name(name){ //added
    }

    uint number() const{ //uncommented
        return m_number; //added
    }

    std::string const& name() const{ //uncommented
        return m_name; //added
    }
};

#endif
