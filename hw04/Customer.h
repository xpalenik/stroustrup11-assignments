#ifndef CUSTOMER_H
#define CUSTOMER_H

#include "CustomerInterface.h"
#include "CustomerData.h" //added
#include "Bank.h" //added
#include <string> //added
#include <vector> //added

typedef unsigned int uint;

class BasicCustomer : public CustomerInterface
{
    CustomerData m_customerData;
    Bank* m_bank=nullptr;
    std::vector<AccountInterface*> m_accounts;

public:

    //This is IMHO a design flaw!
    //Mozny konstruktor:
    //BasicCustomer(uint number, Bank* bank, Database* db);

    //I used this constructor instead:
    BasicCustomer(const CustomerData& customerData, Bank* bank); //added

    //Rule of three:
    ~BasicCustomer() = default;
    BasicCustomer (const BasicCustomer&) = delete;
    BasicCustomer& operator=(BasicCustomer) = delete;

    AccountInterface* createAccount() override;
    std::vector<AccountInterface*> const& accounts() override;
    std::string const& name() const override;
    uint number() const override;
};

#endif 
