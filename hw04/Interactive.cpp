/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "Interactive.h"
#include "CustomerInterface.h"
#include "AccountInterface.h"
#include <iostream>
using namespace std;

Interactive::Interactive(BankRegister* bankRegister){
    if (bankRegister==nullptr){
        throw invalid_argument("bankRegister is null");
    }

    m_bankRegister=bankRegister;

    m_commands.emplace("create_bank", &Interactive::create_bank);
    m_commands.emplace("create_customer", &Interactive::create_customer);
    m_commands.emplace("create_account", &Interactive::create_account);
    m_commands.emplace("deposit", &Interactive::deposit);
    m_commands.emplace("withdraw", &Interactive::withdraw);
    m_commands.emplace("transfer", &Interactive::transfer);
    m_commands.emplace("bank_info", &Interactive::bank_info);
    m_commands.emplace("customer_info", &Interactive::customer_info);
    m_commands.emplace("account_info", &Interactive::account_info);
}

void Interactive::execute(const std::string& command, const std::string& arguments){
    if ( m_commands.find(command) != m_commands.end() ){
        m_commands[command](*this, istringstream(arguments));
    }
}

void Interactive::create_bank(istringstream&& arguments){

    string bank_name;
    arguments.ignore(1);
    getline(arguments,bank_name);

    Bank* bank = new Bank(bank_name, m_bankRegister);
    m_banks.push_back(bank);

    cout << "Created bank: "
         << bank->name()
         << " | "
         << bank->number()
         << endl;
}

void Interactive::create_customer(istringstream&& arguments)const{

    uint bank_number;
    arguments>>bank_number;

    if (arguments.fail()){
        cerr<<"Invalid bank number\n";
        return;
    }

    string customer_name;
    arguments.ignore(1);
    getline(arguments,customer_name);

    if (arguments.fail()){
        cerr<<"Invalid customer name\n";
        return;
    }

    CustomerInterface* customer =
            m_bankRegister->bankByNumber(bank_number)->createCustomer(customer_name);

    cout << "Created customer: "
         << customer->name()
         << " | "
         << customer->number()
         << endl;
}

void Interactive::create_account(std::istringstream&& arguments)const{
    uint bank_number;
    arguments>>bank_number;

    if (arguments.fail()){
        cerr<<"Invalid bank number\n";
        return;
    }

    uint customer_number;
    arguments>>customer_number;

    if (arguments.fail()){
        cerr<<"Invalid customer number\n";
        return;
    }

    /*

    //This is wrong, because

    //"vo funkcii Bank::createAccount() nie je mozne oznamit BasicCustomer,
    //ze si ma pridat AccountInterface* do svojho vektora. Nie je to mozne
    //nijak vykonat, lebo BasicCustomer na to nema verejnu metodu. Predpokladam
    //teda, ze ucet sa vzdy bude vytvarat volanim BasicCustomer::createAccount
    //a nie Bank::createAccount!!!"

    AccountInterface* account=
            m_bankRegister->bankByNumber(bank_number)
                          ->createAccount(customer_number);
    */

    AccountInterface* account=
            m_bankRegister->bankByNumber(bank_number)
                          ->customerByNumber(customer_number)
                          ->createAccount();

    cout << "Created account: "
         << account->number()
         << "/"
         << bank_number
         << endl;
}

void Interactive::deposit(std::istringstream&& arguments)const{
    uint amount;
    arguments>>amount;

    if (arguments.fail()){
        cerr<<"Invalid amount\n";
        return;
    }

    uint account_number;
    arguments>>account_number;

    if (arguments.fail()){
        cerr<<"Invalid account number\n";
        return;
    }

    char delimiter;
    arguments>>delimiter;

    if (arguments.fail()){
        cerr<<"No delimiter found\n";
        return;
    }

    uint bank_number;
    arguments>>bank_number;

    if (arguments.fail()){
        cerr<<"Invalid bank number\n";
        return;
    }

    m_bankRegister->bankByNumber(bank_number)
    ->accountByNumber(account_number)->deposit(amount);
}

void Interactive::withdraw(std::istringstream&& arguments)const{
    uint amount;
    arguments>>amount;

    if (arguments.fail()){
        cerr<<"Invalid amount\n";
        return;
    }

    uint account_number;
    arguments>>account_number;

    if (arguments.fail()){
        cerr<<"Invalid account number\n";
        return;
    }

    char delimiter;
    arguments>>delimiter;

    if (arguments.fail()){
        cerr<<"No delimiter found\n";
        return;
    }

    uint bank_number;
    arguments>>bank_number;

    if (arguments.fail()){
        cerr<<"Invalid bank number\n";
        return;
    }

    m_bankRegister->bankByNumber(bank_number)
    ->accountByNumber(account_number)->withdraw(amount);
}

void Interactive::transfer(std::istringstream&& arguments)const{

    char delim;
    uint amount, account_number1, bank_number1, account_number2, bank_number2;
    arguments >> amount>> account_number1 >> delim >> bank_number1 >> account_number2 >> delim >> bank_number2;

    //throws NonexistentBank
    //throws everything Bank::transfer throws
    m_bankRegister->bankByNumber(bank_number1)
            ->transfer(amount,
                       account_number1,
                       AccountID(account_number2,bank_number2)
                       );
}

void Interactive::bank_info(std::istringstream&& arguments)const{
    uint bank_number;
    arguments>>bank_number;

    Bank* bank = m_bankRegister->bankByNumber(bank_number);

    cout<<"Name: "<<bank->name()<<endl;
    cout<<"Customers: "<<bank->customers().size()<<endl;
    cout<<"Accounts: "<<bank->accounts().size()<<endl;
}

void Interactive::customer_info(std::istringstream&& arguments)const{
    uint bank_number, customer_number;
    arguments >> bank_number >> customer_number;

    CustomerInterface* customer =
            m_bankRegister->bankByNumber(bank_number)
            ->customerByNumber(customer_number);

    cout<<"Name: "<<customer->name()<<endl;
    cout<<"Accounts: ";

    bool first=true;
    for ( auto account : customer->accounts() ){
        if (first){
            cout<<account->number();
            first=!first;
            continue;
        }
        cout<<", "<<account->number();
    }
    cout<<endl;
}

void Interactive::account_info(std::istringstream&& arguments)const{
    char delimiter;
    uint account_number, bank_number;
    arguments >> account_number >> delimiter >> bank_number;

    uint balance = m_bankRegister->bankByNumber(bank_number)
                                 ->accountByNumber(account_number)
                                 ->balance();
    cout << "Balance: " << balance << endl;
}
