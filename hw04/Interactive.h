/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#ifndef INTERACTIVE_H
#define INTERACTIVE_H

#include <map>
#include <sstream>
#include <string>
#include <functional>
#include "BankRegister.h"
#include <stdexcept>
#include "Bank.h"

class Interactive{

    std::map<std::string,
             std::function<void (Interactive&, std::istringstream&&)>
            > m_commands;
    BankRegister* m_bankRegister;
    std::vector<Bank*> m_banks;

    void create_bank(std::istringstream&& arguments); //not const - inserts bank
    void create_customer(std::istringstream&& arguments)const; //done
    void create_account(std::istringstream&& arguments)const; //done
    void deposit(std::istringstream&& arguments)const; //done
    void withdraw(std::istringstream&& arguments)const; //done
    void transfer(std::istringstream&& arguments)const; //done
    void bank_info(std::istringstream&& arguments)const; //done
    void customer_info(std::istringstream&& arguments)const; //done
    void account_info(std::istringstream&& arguments)const; //done

public:

    Interactive(BankRegister*);

    //Rule of three:
    Interactive(const Interactive&) = delete;
    Interactive& operator=(Interactive) = delete;
    ~Interactive(){
        for (Bank* bank : m_banks){
            delete bank;
        }
    }

    void execute(const std::string& command, const std::string& arguments);
};



#endif
