#ifndef DATABASE_H
#define DATABASE_H

#include "CustomerData.h"
#include "AccountData.h"

#include <string>
#include <map>

typedef unsigned int uint;

class Database
{
    uint m_next_ownerNumber   = 1; //added
    uint m_next_accountNumber = 1; //added

    std::map<uint, CustomerData> m_customers; //added
    std::map<uint, AccountData>  m_accounts;  //added
public:
    Database() = default;

    //Rule of three:
    ~Database() = default; //added
    Database (const Database&) = delete;
    Database& operator=(Database) = delete;

    CustomerData createCustomer(std::string const& name);
    AccountData createAccount(uint ownerNumber);

    CustomerData loadCustomer(uint number) const;
    AccountData loadAccount(uint number) const;

    void save(CustomerData);
    void save(AccountData);

    std::map<uint, CustomerData> const& customers() const;
    std::map<uint, AccountData> const& accounts() const;
};

#endif
