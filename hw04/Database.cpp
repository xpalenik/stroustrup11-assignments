/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "Database.h"
#include "BankExceptions.h" //added
#include <iostream> //added
using std::cerr; //added

CustomerData Database::createCustomer(std::string const& name){
    return CustomerData(m_next_ownerNumber++, name);
}

AccountData Database::createAccount(uint){
    return AccountData(m_next_accountNumber++);
}

CustomerData Database::loadCustomer(uint number) const{
    auto result = m_customers.find(number);

    if (result != m_customers.end()){
        return result->second;
    }

    throw NonexistentCustomer();
}

AccountData Database::loadAccount(uint number) const{
    auto result = m_accounts.find(number);

    if (result != m_accounts.end()){
        return result->second;
    }

    throw NonexistentAccount();
}

void Database::save(CustomerData customerData){
    if (! m_customers.emplace(customerData.number(),
                              customerData)
                     .second){
        cerr<<"Error saving CustomerData in Database.\n";
        cerr<<"A customer with the given number probably exists.\n";
    }
}

void Database::save(AccountData accountData){
    if (! m_accounts.emplace(accountData.number(),
                             accountData)
                    .second){
        cerr<<"Error saving AccountData in Database.\n";
        cerr<<"An account with the given number probably exists.\n";
    }
}

std::map<uint, CustomerData> const& Database::customers() const{
    return m_customers;
}

std::map<uint, AccountData> const& Database::accounts() const{
    return m_accounts;
}
