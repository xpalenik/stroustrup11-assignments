/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 * Uloha:       hw04
 */

#include "Account.h"
#include "BankExceptions.h"

//---class BasicAccount--->

void BasicAccount::deposit(uint amount){
    m_balance+=amount;
}

void BasicAccount::withdraw(uint amount){

    if (m_balance >= amount){
        m_balance -= amount;
    } else {
        throw InsufficientFunds();
    }
}

void BasicAccount::transfer(uint amount, AccountID target){
    m_bank->transfer(amount,m_accountData.number(),target);
}
