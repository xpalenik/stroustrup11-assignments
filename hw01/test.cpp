/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * except the Catch framework!
 * @version     1.0
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "codermanchester.h"
#include "codernrzi.h"
#include "coderrz.h"

#include <string>
#include <vector>
#include <utility>
using namespace std;

/*
OK - vstup bol úspešne zakódovaný/dekódovaný
INVALID_CHARACTER - vstup obsahuje neznámy znak
INVALID_FORMAT - vstup obsahuje postupnosť znakov, ktorá nie je daným kódovaním dekódovateľná
INVALID_CHARACTER_SET - v prípade akejkoľvek kódovacej operácie v prípade, že sa zhodujú dva symboly zadané parametrickému konštruktoru
*/

TEST_CASE("Correct return values"){
    CoderManchester cm;
    CoderRZ rz;
    CoderNRZI nrzi;

    string out;

    //INVALID CHARACTER

    SECTION("invalid character for Manchester"){
        REQUIRE(cm.decode("x",out)==INVALID_CHARACTER);
        REQUIRE(cm.decode("0",out)==INVALID_CHARACTER);
    }

    SECTION("invalid character for Rz"){
        REQUIRE(rz.decode("x",out)==INVALID_CHARACTER);
        REQUIRE(rz.decode("-",out)==INVALID_CHARACTER);
    }

    SECTION("invalid character for NRZI"){
        REQUIRE(nrzi.decode("x",out)==INVALID_CHARACTER);
        REQUIRE(nrzi.decode("-",out)==INVALID_CHARACTER);
    }

    //INVALID_FORMAT

    SECTION("invalid format for Manchester"){
        REQUIRE(cm.decode("-",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("+",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("++",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("--",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("+-+",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("+--",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("-++",out)==INVALID_FORMAT);
        REQUIRE(cm.decode("-+-",out)==INVALID_FORMAT);
    }

    SECTION("invalid format for Rz"){
        REQUIRE(rz.decode("+",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("0",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("++",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("0+",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("+00",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("+0+",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("000",out)==INVALID_FORMAT);
        REQUIRE(rz.decode("00+",out)==INVALID_FORMAT);
    }

    SECTION("invalid format for Nrzi"){
        REQUIRE(nrzi.decode("+",out)==INVALID_FORMAT);
        REQUIRE(nrzi.decode("0",out)==INVALID_FORMAT);
    }
}

TEST_CASE("CoderManchester encode/decode does not change output if the input is an empty string"){
    CoderManchester cm;
    string out;

    REQUIRE(cm.encode("",out)==OK);
    REQUIRE(out.empty());
    REQUIRE(cm.getLastTranslatedLength()==0);

    out.clear();

    REQUIRE(cm.decode("",out)==OK);
    REQUIRE(out.empty());
    REQUIRE(cm.getLastTranslatedLength()==0);
}

TEST_CASE("CoderRZ encode/decode does not change output if the input is an empty string"){
    CoderRZ rz;
    string out;

    REQUIRE(rz.encode("",out)==OK);
    REQUIRE(out.empty());
    REQUIRE(rz.getLastTranslatedLength()==0);

    out.clear();

    REQUIRE(rz.decode("",out)==OK);
    REQUIRE(out.empty());
    REQUIRE(rz.getLastTranslatedLength()==0);
}

TEST_CASE("CoderNRZI encode/decode does not change output if the input is an empty string"){
    CoderNRZI nrzi;
    string out;

    REQUIRE(nrzi.encode("",out)==OK);
    REQUIRE(out.empty());
    REQUIRE(nrzi.getLastTranslatedLength()==0);

    out.clear();

    REQUIRE(nrzi.decode("",out)==OK);
    REQUIRE(out.empty());
    REQUIRE(nrzi.getLastTranslatedLength()==0);
}

TEST_CASE("Two same characters as parameters in constructor"){
    string out;

    //constructor is (char positive, char negative, char zero)

//Manchester +-

    CoderManchester a('x','x','y'); //same
    REQUIRE(a.decode("anything",out)==INVALID_CHARACTER_SET);

    CoderManchester b('x','y','x');
    REQUIRE(b.decode("anything",out)==INVALID_CHARACTER);

    CoderManchester c('x','y','y');
    REQUIRE(c.decode("anything",out)==INVALID_CHARACTER);

//NRZI 0+

    CoderNRZI d('x','x','y');
    REQUIRE(d.decode("anything",out)==INVALID_CHARACTER);

    CoderNRZI e('x','y','x');//same
    REQUIRE(e.decode("anything",out)==INVALID_CHARACTER_SET);

    CoderNRZI f('x','y','y');
    REQUIRE(f.decode("anything",out)==INVALID_CHARACTER);

//RZ 0+

    CoderRZ g('x','x','y');
    REQUIRE(g.decode("anything",out)==INVALID_CHARACTER);

    CoderRZ h('x','y','x');//same
    REQUIRE(h.decode("anything",out)==INVALID_CHARACTER_SET);

    CoderRZ i('x','y','y');
    REQUIRE(i.decode("anything",out)==INVALID_CHARACTER);
}

/*RZ
  Príklad:
  reťazec +000+0 sa dekóduje na 101,
  +0+0 sa dekóduje na 11,
  reťazce +000+, +000++ a 0+ nie sú korektné
*/

TEST_CASE("decode and encode correct inputs [CoderRZ]", "[CoderRZ]"){
    vector<pair<string,string>> cases={
        make_pair("+000+0","101"),
        make_pair("+0+0","11"),
        make_pair("","")
    };

    CoderRZ rz;
    string in, out;

    for (auto c:cases){

        REQUIRE(rz.decode(c.first,out)==OK);
        REQUIRE(out==c.second);
        REQUIRE(out.size()==c.second.size());
        REQUIRE(out.size()==rz.getLastTranslatedLength());

        in=out;
        out.clear();

        REQUIRE(rz.encode(c.second,out)==OK);
        REQUIRE(out==c.first);
        REQUIRE(out.size()==c.first.size());
        REQUIRE(out.size()==rz.getLastTranslatedLength());

        out.clear();
    }
}


/*NRZI
Príklad:
reťazce 0000 a ++++ sa dekódujú na 000,
reťazce 0+0+ a +0+0 sa dekódujú na 111,
reťazec +00+ sa dekóduje na 101,
reťazec +0+ sa dekóduje na 11,
reťazec +--+ nie je korektný
Dekódovanie reťazcov + a 0 nemusíte riešiť.
*/

TEST_CASE("decode and encode correct inputs[CoderNRZI]", "[CoderNRZI]"){
    vector<pair<string,string>> cases={
        make_pair("00","0"),
        make_pair("0+","1"),
        make_pair("0000","000"),
        make_pair("0+0+","111")
    };

    CoderNRZI cnrzi;
    string in, out;

    for (auto c:cases){

        REQUIRE(cnrzi.decode(c.first,out)==OK);
        REQUIRE(out==c.second);
        REQUIRE(out.size()==c.second.size());
        REQUIRE(out.size()==cnrzi.getLastTranslatedLength());

        in=out;
        out.clear();

        REQUIRE(cnrzi.encode(c.second,out)==OK);
        REQUIRE(out==c.first);
        REQUIRE(out.size()==c.first.size());
        REQUIRE(out.size()==cnrzi.getLastTranslatedLength());

        out.clear();
    }
}

/*MANCHESTER
Príklad:
vstupný reťazec +--+ sa dekóduje ako 01,
reťazec -+-+ sa dekóduje na 11,
reťazec +-+--+ ako 001,
reťazce +-- a ++ nie sú korektné
*/

TEST_CASE("decode and encode correct inputs[CoderManchester]", "[CoderManchester]"){
    vector<pair<string,string>> cases={
        make_pair("+-","0"),
        make_pair("-+","1"),
        make_pair("-+-+","11"),
        make_pair("-++--+","101"),
        make_pair("",""),
        make_pair("+--++--+","0101")
    };

    CoderManchester cm;
    string in, out;

    SECTION( "decodes and encodes back" ){
        for (auto c:cases){
            REQUIRE(cm.decode(c.first,out)==OK);
            REQUIRE(out==c.second);
            REQUIRE(out.size()==c.second.size());
            REQUIRE(out.size()==cm.getLastTranslatedLength());

            in=out;
            out.clear();

            REQUIRE(cm.encode(in,out)==OK);
            REQUIRE(out==c.first);
            REQUIRE(out.size()==c.first.size());
            REQUIRE(out.size()==cm.getLastTranslatedLength());

            out.clear();
        }
    }
}


TEST_CASE("Do encode and decode use the attributes positive, negative, zero when created by constructor with attributes?","[CoderManchester]"){
    CoderManchester cm('p','n','z');
    string s;

    SECTION("encode"){
        REQUIRE(cm.encode("010",s)==OK);
        REQUIRE(s=="pnnppn");
    }

    SECTION("decode"){
        REQUIRE(cm.decode("pnnppn",s)==OK);
        REQUIRE(s=="010");
    }
}

TEST_CASE("Invalid inputs in encode","[CoderManchester]"){
    vector<pair<string,ErrorCode>> cases={
        make_pair("+d",INVALID_CHARACTER),
        make_pair("+-+",INVALID_CHARACTER),
        make_pair("+-x-",INVALID_CHARACTER)
    };
    CoderManchester cm;
    string s;

    SECTION("checks if correct ErrorCode is returned when given an invalid input"){
        for (auto c:cases){
            REQUIRE(cm.encode(c.first,s)==c.second);
        }
    }
}

TEST_CASE("Invalid inputs in decode","[CoderManchester]"){
    vector<pair<string,ErrorCode>> cases={
        make_pair("+",INVALID_FORMAT),
        make_pair("+-+",INVALID_FORMAT),
        make_pair("+-x-",INVALID_CHARACTER)
    };
    CoderManchester cm;
    string s;

    SECTION("checks if correct ErrorCode is returned when given an invalid input"){
        for (auto c:cases){
            REQUIRE(cm.decode(c.first,s)==c.second);
        }
    }
}

TEST_CASE( "CoderManchester::getLastTranslatedLength", "[CoderManchester]" ) {

    CoderManchester cm;

    SECTION( "Initial lastTranslatedLength should be zero" ){
        REQUIRE(cm.getLastTranslatedLength()==0);
    }
}


TEST_CASE( "CoderManchester::decode and CoderManchester::getLastTranslatedLength", "[CoderManchester]" ) {
    CoderManchester cm;
    REQUIRE(cm.getLastTranslatedLength()==0);

    SECTION( "01 <- +--+" ) {
        const string i("+--+");
        string o;
        REQUIRE(cm.decode(i,o)==OK);
        REQUIRE(o=="01");
        REQUIRE(cm.getLastTranslatedLength()==2);
    }

    SECTION("11 <- -+-+"){
        const string i("-+-+");
        string o;
        REQUIRE(cm.decode(i,o)==OK);
        REQUIRE(o=="11");
        REQUIRE(cm.getLastTranslatedLength()==2);
    }

    SECTION("001 <- +-+--+"){
        const string i("+-+--+");
        string o;
        REQUIRE(cm.decode(i,o)==OK);
        REQUIRE(o=="001");
        REQUIRE(cm.getLastTranslatedLength()==3);
    }

    SECTION("+-- is incorrect"){
        const string i("+--");
        string o;
        REQUIRE(cm.decode(i,o)==INVALID_FORMAT);
        //SHOULD I MODIFY OUTPUT? (empty it, or leave it in the previous state)
        //prípad, že operácia skončila neúspešne [getLastTranslatedLength()] nie je ošetrený (a teda nebude testovaný).
    }

    SECTION("++ is incorrect"){
        const string i("++");
        string o;
        REQUIRE(cm.decode(i,o)==INVALID_FORMAT);
        //SHOULD I MODIFY OUTPUT? (empty it, or leave it in the previous state)
        //prípad, že operácia skončila neúspešne [getLastTranslatedLength()] nie je ošetrený (a teda nebude testovaný).
    }
}

TEST_CASE( "CoderManchester::encode and CoderManchester::getLastTranslatedLength", "[CoderManchester]" ) {
    CoderManchester cm;
    REQUIRE(cm.getLastTranslatedLength()==0);

    SECTION( "01 -> +--+" ) {
        const string i("01");
        string o;
        REQUIRE(cm.encode(i,o)==OK);
        REQUIRE(o=="+--+");
        REQUIRE(cm.getLastTranslatedLength()==4);
    }

    SECTION("11 -> -+-+"){
        const string i("11");
        string o;
        REQUIRE(cm.encode(i,o)==OK);
        REQUIRE(o=="-+-+");
        REQUIRE(cm.getLastTranslatedLength()==4);
    }

    SECTION("001 -> +-+--+"){
        const string i("001");
        string o;
        REQUIRE(cm.encode(i,o)==OK);
        REQUIRE(o=="+-+--+");
        REQUIRE(cm.getLastTranslatedLength()==6);
    }

}
/*
TEST_CASE( "CoderManchester constructors work", "[CoderManchester]" ) {

    SECTION( "Construction without parameters" ) {
        CoderManchester cm;

        REQUIRE(cm.zero=='0');
        REQUIRE(cm.positive=='+');
        REQUIRE(cm.negative=='-');
    }

    SECTION( "Construction with 3 parameters (positive, negative, zero)" ) {
        CoderManchester cm('a','b','c');

        REQUIRE(cm.zero=='c');
        REQUIRE(cm.positive=='a');
        REQUIRE(cm.negative=='b');
    }
}
*/
