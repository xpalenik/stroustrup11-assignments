/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * @version     1.0
 */

#ifndef CODER_MANCHESTER_H
#define CODER_MANCHESTER_H

#include "coder.h"

class CoderManchester: public Coder{

    char positive;
    char negative;
    char zero;
    size_t lastTranslatedLength=0;

public:

    /**
     * @brief Constructor without parameters.
     * pre interpretáciu signálu sa použijú predvolené symboly: symbol + pre kladnú amplitúdu, symbol - pre zápornú a symbol 0 (zero) pre nulový signál.
     */
    CoderManchester();

    /**
     * @brief Constructor with 3 parameters.
     * @param positive The character to represent positive amplitude
     * @param negative The character to represent negative amplitude
     * @param zero The character to represent zero amplitude
     * v základnej verzii môžete predpokladať, že zadané symboly sú navzájom rôzne
     */
    CoderManchester(char positive, char negative, char zero);

   /**
    * @return počet výstupných znakov poslednej operácie (zakódovania/dekódovania)
    * prípad, že operácia skončila neúspešne nie je ošetrený (a teda nebude testovaný)
    */
    size_t getLastTranslatedLength();


   /**
    * @brief Skontroluje reťazec a dekôduje ak je to možné.
    * @param input Vstupný reťazec (zo znakov positive,negative)
    * @param output Výstupný reťazec binárnych číslic (znaky '0' a '1')
    * @return výsledok dekódovania
    * skontroluje, či je možné reťazec input dekódovať (teda či sa skladá len zo znakov "this->positive" a "this->negative").
    * Príklad:
    * vstupný reťazec +--+ sa dekóduje ako 01,
    * reťazec -+-+ sa dekóduje na 11,
    * reťazec +-+--+ ako 001,
    * reťazce +-- a ++ nie sú korektné
    */
    ErrorCode decode(const std::string& input, std::string& output);

    /**
     * @brief Skontroluje reťazec a zakóduje ak je to možné.
     * @param input Vstupný reťazec binárnych číslic (znaky '0' a '1')
     * @param output Výstupný reťazec (zo znakov positive a negative)
     * @return výsledok zakódovania
     * skontroluje, či je možné reťazec input zakódovať (teda či sa skladá len zo znakov '1' a '0').
     * Príklad:
     * 01 -> +--+,
     * 11 -> -+-+ ,
     * 001 -> +-+--+
     */
     ErrorCode encode(const std::string& input, std::string& output);
};

#endif // CODER_MANCHESTER_H
