/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * @version     1.0
 */

#include "coderrz.h"

#include <iostream> //for 1.Bonus
using std::cerr;
using std::endl;

CoderRZ::CoderRZ():CoderRZ('+','-','0'){
}

CoderRZ::CoderRZ(char positive, char negative, char zero):positive(positive),
                                                          negative(negative),
                                                          zero(zero){
    //1.Bonus
    //RZ uses only zero and positive
    if (zero==positive){
        cerr << "RZ recieved same characters." << endl;
    }
}

size_t CoderRZ::getLastTranslatedLength(){
    return lastTranslatedLength;
}

ErrorCode CoderRZ::decode(const std::string& input, std::string& output){

    output.clear();

    if (zero==positive){
        return INVALID_CHARACTER_SET;
    }

    lastTranslatedLength=0;

    //RZ (1 +0)(0 00)
    enum State {MAYBE0, //previous character was '0', after recieving '0' we decoded a '0'
                MAYBE1, //previous character was '+', after recieving '0' we decoded a '1'
                CORRECT}; //nothing recieved yet or all couples ('+0','00') decoded correctly so far

    State state=CORRECT;

    for (char c:input){
        switch (state){
            case CORRECT:
                {
                    if (c==zero){
                        state=MAYBE0;
                        break;
                    }

                    if (c==positive){
                        state=MAYBE1;
                        break;
                    }

                    return INVALID_CHARACTER;
                }

            case MAYBE0: //(0 00)
                {
                    if (c==zero){
                        output.push_back('0');
                        state=CORRECT;
                        ++lastTranslatedLength;
                        break;
                    }

                    if (c==positive){
                        return INVALID_FORMAT;
                    }

                    return INVALID_CHARACTER;
                }

            case MAYBE1: //(1 +0)
                {
                    if (c==zero){
                        output.push_back('1');
                        state=CORRECT;
                        ++lastTranslatedLength;
                        break;
                    }

                    if (c==positive){
                        return INVALID_FORMAT;
                    }

                    return INVALID_CHARACTER;
                }
        }
    }

    return (state==CORRECT)?OK:INVALID_FORMAT;
}

ErrorCode CoderRZ::encode(const std::string& input, std::string& output){

    output.clear();

    if (zero==positive){
        return INVALID_CHARACTER_SET;
    }

    lastTranslatedLength=0;

    for (char c:input){
        switch (c){
            case '1': //RZ (1 +0)
                output.push_back(positive);
                output.push_back(zero);
                lastTranslatedLength+=2;
                break;
            case '0': //RZ (0 00)
                output.push_back(zero);
                output.push_back(zero);
                lastTranslatedLength+=2;
                break;
            default:
                return INVALID_CHARACTER;
        }
    }

    return OK;
}





