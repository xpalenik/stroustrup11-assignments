/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * @version     1.0
 */

#include "codernrzi.h"

#include <iostream> //for 1.Bonus
using std::cerr;
using std::endl;

CoderNRZI::CoderNRZI():CoderNRZI('+','-','0'){
}

CoderNRZI::CoderNRZI(char positive, char negative, char zero):positive(positive),
                                                              negative(negative),
                                                              zero(zero){
    //1.Bonus
    //NRZI uses only zero and positive
    if (zero==positive){
        cerr << "NRZI recieved same characters." << endl;
    }
}

size_t CoderNRZI::getLastTranslatedLength(){
    return lastTranslatedLength;
}

ErrorCode CoderNRZI::decode(const std::string& input, std::string& output){

    output.clear();

    if (zero==positive){
        return INVALID_CHARACTER_SET;
    }

    //not necessary nor tested but I've gone too far to omit this :)
    //"+" and "0" are invalid inputs in NRZI
    if (
            (input.size()==1) &&
                (
                    (*input.c_str()==positive) || (*input.c_str()==zero)
                )
       ){
        return INVALID_FORMAT;
    }

    lastTranslatedLength=0;

    //NRZI (1 10|01)(0 00|11)
    enum State {ZERO, //previous character was '0'
                POSITIVE, //previous character was '+'
                INITIAL //no input has been given yet
               };

    State state=INITIAL;

    for (char c:input){
        switch (state){
            case INITIAL:
                {
                    if (c==zero){
                        state=ZERO;
                        break;
                    }

                    if (c==positive){
                        state=POSITIVE;
                        break;
                    }

                    return INVALID_CHARACTER;
                }

            case ZERO:
                {
                    if (c==zero){
                        output.push_back('0');
                        ++lastTranslatedLength;
                        break;
                    }

                    if (c==positive){
                        output.push_back('1');
                        state=POSITIVE;
                        ++lastTranslatedLength;
                        break;
                    }

                    return INVALID_CHARACTER;
                }

            case POSITIVE:
                {
                    if (c==zero){
                        output.push_back('1');
                        state=ZERO;
                        ++lastTranslatedLength;
                        break;
                    }

                    if (c==positive){
                        output.push_back('0');
                        ++lastTranslatedLength;
                        break;
                    }

                    return INVALID_CHARACTER;
                }
        }
    }

    return OK;
}

ErrorCode CoderNRZI::encode(const std::string& input, std::string& output){

    output.clear();

    if (zero==positive){
        return INVALID_CHARACTER_SET;
    }

    lastTranslatedLength=0;

    if (input.empty()){
        return OK;
    } else {
        output.push_back(zero);
        ++lastTranslatedLength;
    }

    for (char c:input){
        switch (c){
            case '1':
                (output.back()==zero)?output.push_back(positive):output.push_back(zero);
                ++lastTranslatedLength;
                break;
            case '0':
                (output.back()==zero)?output.push_back(zero):output.push_back(positive);
                ++lastTranslatedLength;
                break;
            default:
                return INVALID_CHARACTER;
        }
    }

    return OK;
}



