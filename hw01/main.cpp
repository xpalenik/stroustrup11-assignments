#include "codermanchester.h"
#include "codernrzi.h"
#include "coderrz.h"

#include <iostream>
#include <string>
using std::cout;
using std::cin;
using std::string;
using std::endl;


int main(){
    string in,binaryCm,binaryRz,binaryNrzi, signal;

    CoderManchester cm;
    CoderRZ rz;
    CoderNRZI nrzi;

    char manchesterOk=false;
    char rzOk=false;
    char nrziOk=false;

    cin>>in;

    while (in!="q"){

        if (cm.decode(in,binaryCm)==OK){
            manchesterOk=true;
            cout<<"Manchester: \t"<<binaryCm<<" ["<<cm.getLastTranslatedLength()<<"]\n";
        } else {
            cout<<"Manchester: \tnie je mozne dekodovat\n";
        }

        if (rz.decode(in,binaryRz)==OK){
            rzOk=true;
            cout<<"RZ: \t"<<binaryRz<<" ["<<rz.getLastTranslatedLength()<<"]\n";
        } else {
            cout<<"RZ: \tnie je mozne dekodovat\n";
        }

        if (nrzi.decode(in,binaryNrzi)==OK){
            nrziOk=true;
            cout<<"NRZ-I: \t"<<binaryNrzi<<" ["<<nrzi.getLastTranslatedLength()<<"]\n";
        } else {
            cout<<"NRZ-I: \tnie je mozne dekodovat\n";
        }

        cout<<endl;

        if (manchesterOk && (cm.encode(binaryCm,signal)==OK)){
            cout<<"Manchester: \t"<<signal<<" ["<<cm.getLastTranslatedLength()<<"]\n";
        } else {
            cout<<"Manchester: \tnie je mozne zakodovat\n";
        }

        signal.clear();

        if (rzOk && (rz.encode(binaryRz,signal)==OK)){
            cout<<"RZ: \t"<<signal<<" ["<<rz.getLastTranslatedLength()<<"]\n";
        } else {
            cout<<"RZ: \tnie je mozne zakodovat\n";
        }

        signal.clear();

        if (nrziOk && nrzi.encode(binaryNrzi,signal)==OK){
            cout<<"NRZ-I: \t"<<signal<<" ["<<nrzi.getLastTranslatedLength()<<"]\n";
        } else {
            cout<<"NRZ-I: \tnie je mozne zakodovat\n";
        }

        cout<<endl<<endl;

        manchesterOk=false;
        rzOk=false;
        nrziOk=false;

        cin>>in;
    }


    return 0;
}

