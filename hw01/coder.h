/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * @version     1.0
 */

#ifndef CODER_H
#define CODER_H

#include <cstddef>
#include <string>

/*
OK - vstup bol úspešne zakódovaný/dekódovaný
INVALID_CHARACTER - vstup obsahuje neznámy znak
INVALID_FORMAT - vstup obsahuje postupnosť znakov, ktorá nie je daným kódovaním dekódovateľná
INVALID_CHARACTER_SET - v prípade akejkoľvek kódovacej operácie v prípade, že sa zhodujú dva symboly zadané parametrickému konštruktoru
*/
enum ErrorCode {OK, INVALID_CHARACTER, INVALID_FORMAT, INVALID_CHARACTER_SET};

class Coder {
public:
   virtual ErrorCode decode(const std::string& input, std::string& output) = 0;
   virtual ErrorCode encode(const std::string& input, std::string& output) = 0;
   virtual size_t getLastTranslatedLength() = 0;

   virtual ~Coder(){}
};

#endif // CODER_H
