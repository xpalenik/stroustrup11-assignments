/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * @version     1.0
 */

#ifndef CODER_RZ_H
#define CODER_RZ_H

#include "coder.h"

class CoderRZ: public Coder{

    char positive;
    char negative;
    char zero;
    size_t lastTranslatedLength=0;

public:

    /**
     * @brief Constructor without parameters.
     * pre interpretáciu signálu sa použijú predvolené symboly: symbol + pre kladnú amplitúdu, symbol - pre zápornú a symbol 0 (zero) pre nulový signál.
     */
    CoderRZ();

    /**
     * @brief Constructor with 3 parameters.
     * @param positive The character to represent positive amplitude
     * @param negative The character to represent negative amplitude
     * @param zero The character to represent zero amplitude
     * v základnej verzii môžete predpokladať, že zadané symboly sú navzájom rôzne
     */
    CoderRZ(char positive, char negative, char zero);

   /**
    * @return počet výstupných znakov poslednej operácie (zakódovania/dekódovania)
    * prípad, že operácia skončila neúspešne nie je ošetrený (a teda nebude testovaný)
    */
    size_t getLastTranslatedLength();


   /**
    * @brief Skontroluje reťazec a dekôduje ak je to možné.
    * @param input Vstupný reťazec (zo znakov positive,zero)
    * @param output Výstupný reťazec binárnych číslic (znaky '0' a '1')
    * @return výsledok dekódovania
    * skontroluje, či je možné reťazec input dekódovať (teda či sa skladá len zo znakov "this->positive" a "this->zero").
    * Príklad:
    * reťazec +000+0 sa dekóduje na 101,
    * +0+0 sa dekóduje na 11,
    * reťazce +000+, +000++ a 0+ nie sú korektné
    */
    ErrorCode decode(const std::string& input, std::string& output);

    /**
     * @brief Skontroluje reťazec a zakóduje ak je to možné.
     * @param input Vstupný reťazec binárnych číslic (znaky '0' a '1')
     * @param output Výstupný reťazec (zo znakov positive a zero)
     * @return výsledok zakódovania
     * skontroluje, či je možné reťazec input zakódovať (teda či sa skladá len zo znakov '1' a '0').
     */
     ErrorCode encode(const std::string& input, std::string& output);
};

#endif // CODER_RZ_H
