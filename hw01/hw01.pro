TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11
QMAKE_CXXFLAGS += -Wall -Wextra -pedantic

SOURCES += \
    codermanchester.cpp \
    codernrzi.cpp \
    coderrz.cpp \
    test.cpp

HEADERS += \
    coder.h \
    codermanchester.h \
    codernrzi.h \
    coderrz.h \
    catch.hpp

DISTFILES += \
    otazky \
    todo\
    test_input.txt

