/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * @version     1.0
 */

#include "codermanchester.h"

#include <iostream> //for 1.Bonus
using std::cerr;
using std::endl;

CoderManchester::CoderManchester():CoderManchester('+','-','0'){
}

CoderManchester::CoderManchester(char positive, char negative, char zero):positive(positive),
                                                                          negative(negative),
                                                                          zero(zero){
    //1.Bonus
    //Manchester uses only negative and positive
    if (negative==positive){
        cerr << "CoderManchester recieved same characters." << endl;
    }
}

size_t CoderManchester::getLastTranslatedLength(){
    return lastTranslatedLength;
}

ErrorCode CoderManchester::decode(const std::string& input, std::string& output){

    output.clear();

    if (negative==positive){
        return INVALID_CHARACTER_SET;
    }

    lastTranslatedLength=0;

    //MANCHESTER (1 -+)(0 +-)
    enum State {MAYBE0, //previous character was '+', after recieving '-' we decoded a '0'
                MAYBE1, //previous character was '-', after recieving '+' we decoded a '1'
                CORRECT}; //nothing recieved yet or all couples ('+-','-+') decoded correctly so far

    State state=CORRECT;

    for (char c:input){
        switch (state){
            case CORRECT:
                {
                    if (c==negative){
                        state=MAYBE1;
                        break;
                    }

                    if (c==positive){
                        state=MAYBE0;
                        break;
                    }

                    return INVALID_CHARACTER;
                }

            case MAYBE0: //(0 +-)
                {
                    if (c==negative){
                        output.push_back('0');
                        state=CORRECT;
                        ++lastTranslatedLength;
                        break;
                    }

                    if (c==positive){
                        return INVALID_FORMAT;
                    }

                    return INVALID_CHARACTER;
                }

            case MAYBE1: //(1 -+)
                {
                    if (c==positive){
                        output.push_back('1');
                        state=CORRECT;
                        ++lastTranslatedLength;
                        break;
                    }

                    if (c==negative){
                        return INVALID_FORMAT;
                    }

                    return INVALID_CHARACTER;
                }
        }
    }

    return (state==CORRECT)?OK:INVALID_FORMAT;
}

ErrorCode CoderManchester::encode(const std::string& input, std::string& output){

    output.clear();

    if (negative==positive){
        return INVALID_CHARACTER_SET;
    }

    lastTranslatedLength=0;

    for (char c:input){
        switch (c){
            case '1': //MANCHESTER (1 -+)
                output.push_back(negative);
                output.push_back(positive);
                lastTranslatedLength+=2;
                break;
            case '0': //MANCHESTER (0 +-)
                output.push_back(positive);
                output.push_back(negative);
                lastTranslatedLength+=2;
                break;
            default:
                return INVALID_CHARACTER;
        }
    }

    return OK;
}





