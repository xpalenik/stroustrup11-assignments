#include <iostream>
#include <sstream>
using namespace std;
typedef unsigned long long int ULLINT;

int main(int argc, char** argv){
    if (argc<3){
        cout<<"Pouzitie: ./a.out pocetPrekazok pocetMedzier"<<endl;
        return 1;
    }

    istringstream prekazok_in(argv[1]);
    ULLINT prekazok;
    if (!(prekazok_in>>prekazok)){
	cerr<<"Nespravny pocet prekazok";
        return 1;
    }

    istringstream medzier_in(argv[2]);
    ULLINT medzier;	    
    if (!(medzier_in>>medzier)){
        cerr<<"Nespravny pocet medzier";
        return 1;
    }		

    //ULLINT prekazok;
    //cout<<"Zadajte pocet prekazok: ";
    //cin>>prekazok;
    
    //ULLINT medzier;
    //cout<<"\nZadajte pocet medzier medzi prekazkami:";
    //cin>>medzier;

    const ULLINT stlpcov=2+prekazok*(medzier+1)+3;

//PRVY RIADOK #####################
    for (unsigned stlpec=1; stlpec<=stlpcov; stlpec++){
	cout<<"#";
    }
    cout<<endl;

//DRUHY RIADOK #------------------#
    cout<<"#";	
    for (unsigned stlpec=2; stlpec<stlpcov; stlpec++){
	cout<<"-";
    }
    cout<<"#\n";

//TRETI RIADOK #o#---#---#---#---#-#
    cout<<"#o";

    for (unsigned prekazka=1; prekazka<=prekazok; prekazka++){
	cout<<"#";
	for (unsigned medzera=1; medzera<=medzier; medzera++){
	    cout<<"-";
        }
    }

    cout<<"#-#\n";

//STVRTY RIADOK #------------------#
    cout<<"#";	
    for (unsigned stlpec=2; stlpec<stlpcov; stlpec++){
	cout<<"-";
    }
    cout<<"#\n";

//PIATY RIADOK #####################
    for (unsigned stlpec=1; stlpec<=stlpcov; stlpec++){
	cout<<"#";
    }
    cout<<endl;
}   
