TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -pedantic -Wall -Wextra -g

SOURCES +=main.cpp\
    fill.cpp \
    utility.cpp

HEADERS += \
    fill.h \
    utility.h \
    catch.hpp

DISTFILES += \
    todo

