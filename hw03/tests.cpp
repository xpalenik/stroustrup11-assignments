/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "fill.h"
#include "utility.h"
#include <stdexcept>

using namespace std;

TEST_CASE("Error EdgeMap::print() const"){
    EdgeMap e;
    REQUIRE(e.load("fill_basic.dat").getError()==Error::E_OK);
    REQUIRE(e.print().getError() == Error::Err::E_OK);
    REQUIRE(e.m_mapSize.getX()==15);
    REQUIRE(e.m_mapSize.getY()==8);
}

TEST_CASE("Error EdgeMap::load(const string &path)"){
    EdgeMap e;
    REQUIRE(e.load("fill_basic.dat").getError()==Error::E_OK);
    REQUIRE(e.m_mapSize.getX()==15);
    REQUIRE(e.m_mapSize.getY()==8);
    /*
    switch (e.load("fill_basic.dat").getError()){
        case Error::E_CANTOPEN:
            cerr<<"E_CANTOPEN\n";
            break;
        case Error::E_INVALIDCHAR:
            cerr<<"E_INVALIDCHAR\n";
            break;
        case Error::E_SIZE:
            cerr<<"E_SIZE\n";
            break;
        case Error::E_NOSEED:
            cerr<<"E_NOSEED\n";
            break;
        case Error::E_MORESEEDS:
            cerr<<"E_MORESEEDS\n";
            break;
        case Error::E_OK:
            cerr<<"E_OK\n";
            break;
        default:
            cerr<<"other error while loading\n";
            break;
    }
    */
}

TEST_CASE("ushort EdgeMap::getIndex(ushort x, ushort y) const"){
    EdgeMap incorrect;

    SECTION("Incorrect EdgeMap"){ 
        REQUIRE_THROWS(incorrect.getIndex(0,0));
    }

    SECTION("Valid indexes"){
        //TODO
    }

    SECTION("Out of range indexes"){
        //TODO
    }
}


