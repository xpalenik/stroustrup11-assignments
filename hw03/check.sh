#!/bin/bash

examples="examples" #the name of directory with maps

if [ ! -d ${examples} ]; then
	echo -e "Directory with examples not found."
	echo -e "Please create a directory with examples called ${examples}"
	exit
fi

if [ ! "$(ls -A ${examples})" ]; then
	echo -e "Directory with examples is empty."
	exit
fi

echo -e "Removing ./fill, *.gch, difference, out and out_correct."
rm fill *.gch difference out out_correct 2>/dev/null
echo -e "Compiling"
g++ -std=c++11 -pedantic -Wall -Wextra -Werror -o fill main.cpp fill.cpp fill.h utility.cpp utility.h
echo -e "Starting a check of example maps (all *.dat files in directory ${examples})."

passed=true
difference="difference"

for file in ${examples}/*.dat
do
	if [ -d ${file} ]; then
	  continue;
	fi

	echo -n "Checking ${file}..."
	./fill_standard ${file} > out_correct
	./fill ${file} > out
	diff out out_correct > ${difference}
	if [ -s ${difference} ]
	then
     	  passed=false
	  echo -e "failed!"
          echo -e "the difference is:"
	  cat difference
	else
	  echo "ok"
	fi
done

echo -e "Removing the temporary files."
rm difference out out_correct

if $passed; then
  echo "All tests passed."
else
  echo "Some test failed!"
fi
