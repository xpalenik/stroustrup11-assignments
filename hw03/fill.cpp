/**
 * @author      Martin Palenik <359817@mail.muni.cz>
 * Skupina:     PB161/14
 * Cviciaci:    Mgr. Jan Juran
 */

#include "fill.h"
#include <stdexcept>
#include <string>
#include <limits>
using namespace std;

/*****************************************************************************/

bool valid(const char& c){
    return (c==PIXEL_BORDER)||
           (c==PIXEL_BLANK)||
           (c==PIXEL_SEED)||
           (c=='\n');
}

//can catch
//E_INVALIDCHAR
//E_MORESEEDS
Error getError(const string& line, bool& seed){

    for (auto c:line){
        if (!valid(c)){
            return Error(Error::Err::E_INVALIDCHAR);
        }

        if (c==PIXEL_SEED){
            if (seed){
                return Error(Error::Err::E_MORESEEDS);
            } else {
                seed=true;
            }
        }
    }

    return Error(Error::Err::E_OK);
}

Error EdgeMap::load(const string &path)
{
    m_correct=false;

    ifstream file;
    ios_base::iostate state = file.exceptions(); //save state
    file.exceptions(ifstream::failbit | ifstream::badbit);

    try {
        file.open(path);
    }
    catch (ios_base::failure & ex) {
        cerr << "Opening file failed: "<< ex.what() << "\n";
        return Error(Error::Err::E_CANTOPEN);
    }

    file.exceptions(state); //load state

    //reserve size equal to length of file

    file.seekg (0, file.end);
    m_edgeMap.reserve(file.tellg());
    file.seekg (0, file.beg);

    //load the first line
    bool seed=false;
    string line;
    getline(file,line);

    if (line.length() > numeric_limits<ushort>::max()){
        //its just too big, we should not even care!
        //cerr<<"First line too long to be indexed with getIndex() without overflow\n";

        //we're gonna die!!!
        //but why?
        Error ok=getError(line,seed);

        if (!ok){
            return ok; //brainf*ck
        }

        if (!seed){
            //guessing game, maximizing our chances...
            return Error(Error::Err::E_NOSEED);
        }

        return Error(Error::Err::E_SIZE); //why not

        //My opinion:
        //the vzorove riesenie fill_standard fails the test with single extremely long line with seed at the end
        //I implemented this method to handle even this case correctly
        //it works because fill() checks that rows>2 and cols>2 and if not, the color will flow out (E_OUTOFMAP)
        //Hovewer I think this should be sanitized explicitly with another error code or an exception!
        //I returned E_OK because it makes most sense (and fill_standard does the same)
    }

    //'COLUMNS' and 'row' are ushort, because they can't exceed 'numeric_limits<ushort>::max()'
    //otherwise there might be an overflow in getIndex()
    const ushort COLUMNS=line.length();
    ushort row=0;

    if (COLUMNS==0){
        return Error(Error::Err::E_NOSEED);
    }



    do {
        if (line.length()!=COLUMNS){
            //we're gonna die!!!
            //but why?
            Error ok=getError(line,seed);

            if (!ok){
                return ok; //brainf*ck
            }

            //no better reason found *sigh
            return Error(Error::Err::E_SIZE);
        }

        ushort column=0;

        for (auto c:line){
            if (!valid(c)){
                return Error(Error::Err::E_INVALIDCHAR);
            }

            if (c==PIXEL_SEED){
                if (!seed){
                    m_seed=Point(column,row);
                    seed=true;
                } else {
                    return Error(Error::Err::E_MORESEEDS);
                }
            }

            m_edgeMap.push_back(c);
            //column cant overflow because its max is column==line.length()<=numeric_limits<ushort>::max()
            ++column;
        }

        //row can overflow. check is necessary!
        if (static_cast<ulong>(row)+1 > numeric_limits<ushort>::max()){
            //this limit is not sanitized in the fill_standard!
            //cerr<<"Too many rows to be indexed with getIndex() without overflow\n";

            //guessing game
            if (!seed){
                return Error(Error::Err::E_NOSEED);
            }

            //cant return ok so returned E_SIZE
            return Error(Error::Err::E_SIZE);
        }
        ++row;

    } while (getline(file,line));

    if (!seed){
        return Error(Error::Err::E_NOSEED);
    }

    m_mapSize=Point(COLUMNS,row);
    m_correct=true;

    return Error(Error::Err::E_OK);
}

/*****************************************************************************/

/**
 * Pseudocode:
 *
 * 1. pop pixel
 * 2. go to the beginning of its segment (<--left)
 * 3. cycle to the right border (right-->) and for each pixel
 *    3.1 fill it
 *    3.2 check the north (top, above, up) pixel; enqueue?
 *    3.3 check the south (bottom, bellow, down) pixel; enqueue?
 */

Error EdgeMap::fill(ushort &area)
{
    area=0;

    if (!m_correct){
        return  Error(Error::Err::E_NOTLOADED);
    }

    //if there is nowhere to hide the seed, it will flew out
    if (m_mapSize.getX()<3 ||
        m_mapSize.getY()<3){
        return  Error(Error::Err::E_OUTOFMAP);
    }

    m_edgeMap[ getIndex(m_seed.getX(),
                        m_seed.getY())
             ] = PIXEL_BLANK;

    DynamicData stack;
    stack.push(m_seed);

    const ushort LAST_COLUMN=m_mapSize.getX()-1;
    const ushort LAST_ROW=m_mapSize.getY()-1;

    while (!stack.empty()){
        Point p;
        stack.pop(p);

        if (p.getX()<=0 ||
            p.getX()>=LAST_COLUMN ||
            p.getY()<=0 ||
            p.getY()>=LAST_ROW){
        return Error(Error::Err::E_OUTOFMAP);
        }

        //is the pixel in filled segment?
        if (m_edgeMap[getIndex(p.getX(),p.getY())]
            == PIXEL_FILLED){
            continue;
        }

        //go to the beginning of its segment (<--left)
        ushort left=p.getX();

        while (m_edgeMap[getIndex(left,p.getY())] != PIXEL_BORDER){
            if (left<=0){
                return Error::E_OUTOFMAP;
            }
            --left;
        }
        ++left;

        bool topSegmentOnStack=false;
        bool bottomSegmentOnStack=false;

        //cycle to the right border (right-->)
        while (m_edgeMap[getIndex(left,p.getY())]
               != PIXEL_BORDER){

            if (left==LAST_COLUMN){
                return Error::E_OUTOFMAP;
            }

            m_edgeMap[getIndex(left,p.getY())]=PIXEL_FILLED;
            ++area;

            //check the north (top, above, up) pixel; enqueue?
            if (p.getY()-1>=0){
                const ushort north=p.getY()-1;

                switch (m_edgeMap[getIndex(left,north)]){
                    case PIXEL_BLANK:
                        if (!topSegmentOnStack){
                            stack.push(Point(left,north));
                            topSegmentOnStack=true;
                            }
                        break;
                    case PIXEL_BORDER:
                        topSegmentOnStack=false;
                        break;
                    case PIXEL_FILLED:
                        break;
                    default:
                        cerr<<"unexpected character in north segment\n";
                        cerr<<"the character is '"<<m_edgeMap[getIndex(left,north)]<<"'\n";
                }
            }

            //check the south (bottom, bellow, down) pixel; enqueue?
            const ushort south=p.getY()+1;
            if (south<m_mapSize.getY()){

                switch (m_edgeMap[getIndex(left,south)]){
                    case PIXEL_BLANK:
                        if (!bottomSegmentOnStack){
                            stack.push(Point(left,south));
                            bottomSegmentOnStack=true;
                            }
                        break;
                    case PIXEL_BORDER:
                        bottomSegmentOnStack=false;
                        break;
                    case PIXEL_FILLED:
                        break;
                    default:
                        cerr<<"unexpected character in south segment\n";
                        cerr<<"the character is '"<<m_edgeMap[getIndex(left,south)]<<"'\n";
                }
            }

            ++left;
        }
    }

    return Error(Error::E_OK);
}

/*****************************************************************************/

Error EdgeMap::print() const
{
    if (!m_correct){
        cerr<<"Incorrect map";
        return Error(Error::Err::E_NOTLOADED);
    }

    for (unsigned row=0; row<m_mapSize.getY(); row++){
        for (unsigned column=0; column<m_mapSize.getX(); column++){
            cout<<m_edgeMap[getIndex(column,row)];
        }
        cout<<endl;
    }

    return Error(Error::Err::E_OK);
}

/*****************************************************************************/

ushort EdgeMap::getIndex(ushort x, ushort y) const
{

    if (!m_correct){
        throw runtime_error("getIndex called on invalid EdgeMap\n");
    }

    //Thou shalt C the overflow (caused by argument promotion ushort->short).
    //Thou shalt beware the dark paths of undefined behaviour and obey the C++11 §4.5/1!
    //http://stackoverflow.com/questions/33731158/why-is-unsigned-short-multiply-unsigned-short-converted-to-signed-int

    const ulong oneD = static_cast<ulong>(x) +
                       static_cast<ulong>(y) * static_cast<ulong>(m_mapSize.getX());
    const ulong size = static_cast<ulong>(m_mapSize.getX()) *
                       static_cast<ulong>(m_mapSize.getY());

    if (oneD>=size){
        throw out_of_range("getIndex: index out of range");
    }

    return oneD;
}

/*****************************************************************************/
